﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UI : MonoBehaviour
{
    [SerializeField] private GameObject _Background;
    [SerializeField] private GameObject _Pause_Panel;
    [SerializeField] private Transform _Defeat_Panel;
    [SerializeField] private Transform _Victory_Panel;     
    [SerializeField] private Ease _Ease;    
    [SerializeField] private Image _Farmer_Health_Image;
    [SerializeField] private TextMeshProUGUI _Bomb_Count_Text;
    private AudioSource _Audio_Source;  
    void Start()
    {
        _Audio_Source = GetComponent<AudioSource>();
        _Audio_Source.Play();
        _Background.SetActive(false);     
        Time.timeScale = 1;
    }
    public void PlayerDefeat()
    {
        Time.timeScale = 0;
        _Background.SetActive(true);
        _Defeat_Panel
            .DOMoveY(360f, 2)
            .SetEase(_Ease)   
            .SetUpdate(true);
    }
    public void PlayerVictory()
    {
        Time.timeScale = 0;
        _Background.SetActive(true);       
        _Victory_Panel
            .DOMoveY(360, 2)
            .SetEase(_Ease)
            .SetUpdate(true);
    }

    public void BombCountText(int _bomb_Count)
    {
        _Bomb_Count_Text.text = "Bomb Count: " + _bomb_Count;
    }
    public void StartAgain()
    {
        StartCoroutine(Reload());      
    }
    private IEnumerator Reload()
    {
        _Background.SetActive(true);
        _Victory_Panel
            .DOMoveY(1000f, 2)
            .SetEase(_Ease)
            .SetUpdate(true);
        _Defeat_Panel
            .DOMoveY(1000f, 2)
            .SetEase(_Ease)
            .SetUpdate(true);
        yield return new WaitForSecondsRealtime(1.5f);
        SceneManager.LoadScene(0);
    }
   
    public void Resume()
    {
        Time.timeScale = 1;
        _Audio_Source.Play();
        _Background.SetActive(false);
        _Pause_Panel.SetActive(false);      
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void Pause()
    {
        Time.timeScale = 0;
        _Audio_Source.Pause();
        _Background.SetActive(true);
        _Pause_Panel.SetActive(true);              
    }
    public void _Farmer_Health(float _health)
    {
        _Farmer_Health_Image.fillAmount = _health / 100;
    }

   
}
