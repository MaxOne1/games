﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _Bomb;
    [SerializeField] private Transform[] _Bomb_Place;
    private bool _Game_Over;
    private AudioSource _Audio_Source;
    void Start()
    {
        _Game_Over = false;
        _Audio_Source = GetComponent<AudioSource>();
        InvokeRepeating("BombCreate", 5f, 10f);
    }

    
    private void BombCreate()
    {
        int _index = Random.Range(0, _Bomb_Place.Length);
        if (!_Game_Over)
        {            
            Instantiate(_Bomb, _Bomb_Place[_index].position, Quaternion.identity);
        }      
    }
    public void PlaySound()
    {
        _Audio_Source.Play();
    }
    public void StopSound()
    {
        _Audio_Source.Stop();
    }
    public void StopGame()
    {
        _Game_Over = true;
    }
}
