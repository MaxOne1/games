using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultRifle : Gun
{
    [SerializeField] private float _Bullet_Spread;

    protected override void BulletSpread(float _bullet_spread)
    {
        base.BulletSpread(_Bullet_Spread);
    }
}
