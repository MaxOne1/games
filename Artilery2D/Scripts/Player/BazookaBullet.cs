﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BazookaBullet : MonoBehaviour
{
    public float force;
    private Rigidbody2D rb;
    public float radius;
    public int damage;
    public GameObject effectObject;
    private PressButton fireButton;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //rb.AddForce(transform.right * force, ForceMode2D.Impulse);
        fireButton = GameObject.FindGameObjectWithTag("FireButton").GetComponent<PressButton>();

    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<TrailRenderer>().enabled = false;      
        if (collision.CompareTag("Ground"))
        {
            Instantiate(effectObject, transform.position, Quaternion.identity);
            Collider2D[] boom = Physics2D.OverlapCircleAll(transform.position, radius);
            CameraShake();
            for (int i =0; i< boom.Length; i++)
            {
                Rigidbody2D rigibody = boom[i].attachedRigidbody;
                if (!rigibody)
                {
                    Destroy(boom[i].gameObject);
                }                                       
            }
            Destroy(gameObject);
        }
        if (collision.CompareTag("Enemy"))
        {
            Instantiate(effectObject, transform.position, Quaternion.identity);
            Destroy(gameObject);
            CameraShake();

        }
        if (collision.CompareTag("Static"))
        {
            Instantiate(effectObject, transform.position, Quaternion.identity);
            Debug.Log("Ground");
            Destroy(gameObject);
        }
    }
    void CameraShake()
    {
        Camera.main.GetComponent<CameraShake>().shakeDuration = 0.2f;
    }
}
