﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepsSounds : MonoBehaviour
{
    [SerializeField] private AudioClip[] _Footsteps;
    [SerializeField] private float _Step_Time;
    private AudioSource _Audio_Source;
    private bl_Joystick _Joystick_Script;
    private bool _Stop_Play;
    private int _Clip_Index;


    private void Start()
    {
        _Audio_Source = GetComponent<AudioSource>();
        _Joystick_Script = GameObject.Find("MoveJoystick").GetComponent<bl_Joystick>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(PlaySteps());
        if (collision.collider.CompareTag("Ground"))
        {
            _Clip_Index = 0;
        }
        if (collision.collider.CompareTag("Wood"))
        {
            _Clip_Index = 1;
        }
    }
    IEnumerator PlaySteps() //Восроизведение шагов
    {
        if(!_Stop_Play && _Joystick_Script._Is_Pressed)
        {
            _Stop_Play = true;
            _Audio_Source.PlayOneShot(_Footsteps[_Clip_Index]);
            yield return new WaitForSeconds(_Step_Time);
            _Stop_Play = false;
        }
       
    }


}
