using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Gun
{
    [SerializeField] private Transform _Second_Muzzle;
    public override void Shoot()
    {
        if (Time.time > _Next_Fire && _Ammunition > 0)
        {
            _Next_Fire = Time.time + 1f / _Fire_Rate;
            Instantiate(_Bullet, _Muzzle.position, _Muzzle.rotation);
            Instantiate(_Bullet, _Second_Muzzle.position, _Second_Muzzle.rotation);
            _Ammunition--;
            FireSound();
        }
        if (_Ammunition <= 0)
        {
            StartCoroutine(Recharge());
        }
    }
    
}
