﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeAdd : MonoBehaviour
{
    [SerializeField] private GameObject _Paricles_Prefab;
    private Player _Player_Script;

    private void Start()
    {
        _Player_Script = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
   
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cube"))
        {
            Instantiate(_Paricles_Prefab, transform.position, Quaternion.identity);
            _Player_Script._Player.transform.position += new Vector3(0, 1, 0);
            this.gameObject.transform.parent = _Player_Script._All_Cubes.transform;
            this.gameObject.transform.position = _Player_Script._Spawn_Pos.position;
            this.gameObject.tag = "Cube";
            Destroy(this);
        }        
    }
}
