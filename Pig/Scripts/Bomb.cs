﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private ParticleSystem boom;
    [SerializeField] float radius;
    [SerializeField] GameObject farmer;
    [SerializeField] protected float damage;
    [SerializeField] private AudioClip boomSound;
    [SerializeField] protected GameObject boomObject;
    [SerializeField] private LayerMask farmerMask;
    private SpriteRenderer spriteRenderer;
    private AudioSource audioSource;
    
    protected Farmer farmerScript;
    void Start()
    {
        farmerScript = GameObject.FindGameObjectWithTag("Farmer").GetComponent<Farmer>();
        audioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(Boom());
    }

   
    IEnumerator Boom()
    {
        InvokeRepeating("DangerColor", 0, 0.6f);
        InvokeRepeating("DefColor", 0.5f, 1);
        yield return new WaitForSeconds(5);
        boom.Play();
        Damage();
        audioSource.PlayOneShot(boomSound);

        Destroy(gameObject,0.6f);
    }
    void Damage()
    {
        Collider2D[] boomCollider = Physics2D.OverlapCircleAll(transform.position, radius, farmerMask);
        foreach(Collider2D coll in boomCollider)
        {            
            farmerScript.health -= damage;
        }

    }
    private void DefColor()
    {
        spriteRenderer.color = Color.white;
    }
    private void DangerColor()
    {
        spriteRenderer.color = Color.red;
    }

}
