﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class UIGameManager : MonoBehaviour
{
    [SerializeField] private GameObject panel;
    public bool _Pause;
    private Controller _Controller_Script;
    [SerializeField]private AudioSource _Safe;
    [SerializeField]private AudioSource _Danger;
    [SerializeField] private AudioSource _Death;
    [SerializeField] private GameObject _Take_Button;
    [SerializeField] private GameObject _Drop_Button;
    [SerializeField] private GameObject _Sit_Down_Button;
    [SerializeField] private GameObject _Break_Button;
    [SerializeField] private GameObject _Game_Over_Panel;
    [SerializeField] private Sprite _Down;
    [SerializeField] private Sprite _Up;
    [SerializeField] private AudioClip _Sream;
    [SerializeField] private AudioClip _Knife;

    private void Start()
    {
        panel.SetActive(false);
        _Pause = false;
        _Controller_Script = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
        _Take_Button.SetActive(false);
        _Drop_Button.SetActive(false);
        _Break_Button.SetActive(false);
        _Danger.mute = true;
        _Game_Over_Panel.SetActive(false);
        Time.timeScale = 1;
    }
    private void Update()
    {
        TakeButton();
        DropButton();
    }

    public void PauseActive()
    {
        panel.SetActive(true);
        Time.timeScale = 0;
        _Pause = true;
    }

    public void ContinueGame()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
        _Pause = false;
    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
        _Pause = false;

    }
    public void Restart()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
        _Pause = false;
    }
    public void TakeButton()
    {
        if (_Controller_Script._Can_Take)
        {
            _Take_Button.SetActive(true);
        }
        else
        {
            _Take_Button.SetActive(false);
        }
    }
    public void DropButton()
    {
        if (_Controller_Script._Have_Item)
        {
            _Drop_Button.SetActive(true);
        }
        else
        {
            _Drop_Button.SetActive(false);
        }   
    }
    public void SitDownButton()
    {
        if (!_Controller_Script._Down)
        {
            _Controller_Script._Down = true;
            _Sit_Down_Button.GetComponent<Image>().sprite = _Down;
            _Controller_Script.SitDown();
        }
        else
        {
            _Controller_Script._Down = false;
            _Sit_Down_Button.GetComponent<Image>().sprite = _Up;
            _Controller_Script.SitDown();
        }
    }  
    public void BreakBoard()
    {
        if(_Controller_Script.hit.collider.gameObject.name == "Door")
        {
            _Break_Button.SetActive(true);
        }
        else
        {
            _Break_Button.SetActive(false);
        }
    }
    public void DangerMusic()
    {
        _Safe.mute = true;
        _Danger.mute = false;
    }
    public void SafeMusic()
    {
       _Safe.mute = false;
       _Danger.mute = true;
    }
    public void Death()
    {
        Time.timeScale = 0;
        _Safe.mute = true;
        _Danger.mute = true;
        _Game_Over_Panel.SetActive(true);
    }
}
