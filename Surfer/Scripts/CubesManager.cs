﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CubesManager : MonoBehaviour
{
   
    private void OnCollisionEnter(Collision collision)
    {       
       
        if (collision.collider.CompareTag("Obstacle"))
        {
            transform.parent = null;
        }      
        if (collision.gameObject.CompareTag("Finish"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (collision.gameObject.CompareTag("Lava"))
        {
            Destroy(gameObject);
        }

    }
}
