﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] private GameObject[] _Player_Cubes;

    [SerializeField] private Transform _Player_Pos;

    [SerializeField] private List<Material> _Cube_Number;
    [SerializeField] private List<int> _List_Of_Numbers;
    private bool _Classic_Mode;
    private bool _Normal_Mode;

    private GameObject[] _Cubes_On_Scene;

    private int _Moves;

    private UIGame _UI_Script;

    private void Start()
    {
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<UIGame>();
        _List_Of_Numbers.Clear();
    }
    public void CallCreateFunction()
    {
        _Moves++;
        _UI_Script.Moves(_Moves);
        Invoke("CreateCube", 1);
    }
    private void MoveScore()
    {
        if(_Moves < PlayerPrefs.GetInt("Moves"))
        {
            PlayerPrefs.SetInt("Moves", _Moves);
        }
    }

    public void ChangeTexture(GameObject _cube, int _index)
    {
        _cube.GetComponent<Renderer>().material = _Cube_Number[_index];
    }
    public void Victory()
    {
        MoveScore();
        _UI_Script.Victory(_Moves);
    }
    public void Defeat()
    {
        _UI_Script.Defeat();
    }
    public void Borders(float _border_X, GameObject _cube)
    {
        if (_cube.transform.position.x > _border_X)
        {
            _cube.transform.position = new Vector3(_border_X, _cube.transform.position.y, _cube.transform.position.z);
        }
        if (_cube.transform.position.x < -_border_X)
        {
            _cube.transform.position = new Vector3(-_border_X, _cube.transform.position.y, _cube.transform.position.z);
        }
    }
    private void CreateRandomCube()
    {
        GetCubesOnScene();
        RandomSelectiom();
    }
    private void CreateSameCubes()
    {
        Instantiate(_Player_Cubes[0], _Player_Pos.position, _Player_Cubes[0].transform.rotation);
    }

   private void CreateCube()
    {
        if (_Normal_Mode)
        {
            CreateRandomCube();
        }
        if (_Classic_Mode)
        {
            CreateSameCubes();
        }
    }
    public void ClassicMode()
    {
        _Classic_Mode = true;
        _Normal_Mode = false;
        _Moves = 0;
    }
    public void NormalMode()
    {
        _Normal_Mode = true;
        _Classic_Mode = false;
        _Moves = 0;
    }
    private void RandomSelectiom()
    {
        int _random_Number = Random.Range(0, _List_Of_Numbers.Count);
        int _random_Cube = 0;
        switch (_List_Of_Numbers[_random_Number])
        {
            case 2:
                _random_Cube = 0;
                break;
            case 4:
                _random_Cube = 1;
                break;
            case 8:
                _random_Cube = 2;
                break;
            case 16:
                _random_Cube = 3;
                break;
            case 32:
                _random_Cube = 4;
                break;
            case 64:
                _random_Cube = 5;
                break;
            case 128:
                _random_Cube = 6;
                break;
            case 256:
                _random_Cube = 7;
                break;
            case 512:
                _random_Cube = 8;
                break;
            case 1024:
                _random_Cube = 9;
                break;
        }
        Instantiate(_Player_Cubes[_random_Cube], _Player_Pos.position, _Player_Cubes[_random_Cube].transform.rotation);
    }
    private void GetCubesOnScene()
    {
        _Cubes_On_Scene = GameObject.FindGameObjectsWithTag("Cube");
        Cube _cube_Script;
        foreach (GameObject _cube in _Cubes_On_Scene)
        {
            _cube_Script = _cube.GetComponent<Cube>();
            for (int i = 0; i < _Cubes_On_Scene.Length; i++)
            {
                if (!_List_Of_Numbers.Contains(_cube_Script._Number))
                    _List_Of_Numbers.Add(_cube_Script._Number);
            }
        }
    }

}
    

