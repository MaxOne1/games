﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.EventSystems;
using UnityEngine.ProBuilder;
using UnityEngine.UI;

public class Controller : MonoBehaviour, IEndDragHandler, IDragHandler
{ 
    //Вешать на панель, не на игрока!
    [SerializeField] private float _Speed; //Скорость передвижения
    [SerializeField] private bl_Joystick _Joystick; //Джойстик передвижения
    [SerializeField] private AudioClip _Step; //Звук шага
    [SerializeField] private float _Step_Time; //Скорость шагов 
    [SerializeField] private GameObject _Player; //"Тело" игрока
    [SerializeField] private GameObject _Arm; //"Рука" игрока 

    private bl_Joystick _Joystick_Script;
    private AudioSource _AudioSource;
    private Rigidbody _Rb;    
    private bool _Stop_Play; //Воспроизведение шага
    private Animator _Animator;

    [SerializeField] private float _Sensitivity; //Чувствительность пальца
    [SerializeField] private GameObject _Head; //"Голова" игрока
    [SerializeField] private bool _Is_Pressed; //Касание паоьцем экрана
    public bool _Can_Take; //Способность взять предмет
    private float _Touch_Ver = 0f;
    private float _Touch_Hor = 0f;
    private int _Touch_Index;
    private GameObject _Item_In_Hand; //Предмет, находящийся в руках игрока
    public RaycastHit hit;
    public bool _Have_Item; //Наличие предмета в руках
    public bool _Down; //Положение "сидя"
    public bool _Dead;
  
    void Start()
    {
        _Rb = GetComponent<Rigidbody>();
        _AudioSource = GetComponent<AudioSource>();
        _Joystick_Script = GameObject.Find("MoveJoystick").GetComponent<bl_Joystick>();
        _Animator = GetComponent<Animator>();
        _Stop_Play = false;
        _Is_Pressed = false;
        _Can_Take = false;
        _Have_Item = false;
        _Down = false;
        _Dead = false;
    }

    void FixedUpdate()
    {
        Movement();
        CanTakeItem();
    }
    private void Movement() //Передвижение игрока
    {
        float _Move_V = _Joystick.Vertical;
        float _Move_H = _Joystick.Horizontal;
        Vector3 _Move = new Vector3(_Move_H, 0, _Move_V);
        _Player.transform.Translate(_Move * _Speed / 4 * Time.fixedDeltaTime);
        if (!_Stop_Play && _Joystick_Script._Is_Pressed)
            StartCoroutine(PlaySteps());

    }
    IEnumerator PlaySteps() //Восроизведение шагов
    {
        _Stop_Play = true;
        _AudioSource.PlayOneShot(_Step);
        yield return new WaitForSeconds(_Step_Time);
        _Stop_Play = false;
    }
    public void CameraMove() //Поворот игрока
    {
       if (Input.touchCount <= 2 && _Is_Pressed)
       {
         _Touch_Hor += Input.GetTouch(_Touch_Index).deltaPosition.x * _Sensitivity * Time.deltaTime;
         _Touch_Ver -= Input.GetTouch(_Touch_Index).deltaPosition.y * _Sensitivity * Time.deltaTime;
         _Touch_Ver = Mathf.Clamp(_Touch_Ver, -90, 90);
         _Head.transform.localRotation = Quaternion.Euler(_Touch_Ver, 0f, 0f);
         _Player.transform.rotation = Quaternion.Euler(0, _Touch_Hor, 0);
       }                      
             
    }
    
    private void IsPressedOn() //Есть нажатие на экран
    {
        _Is_Pressed = true;      
    }
  
    private void IsPressedOff() //Нет нажатия на экран
    {
        _Is_Pressed = false;
        _Touch_Index = 0;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        IsPressedOff();      
    }

    public void OnDrag(PointerEventData eventData)
    {
        IsPressedOn();
        CameraMove();
        _Touch_Index = eventData.pointerId;
        if (eventData.pointerId >= 2) // Вычисление пальца, управляющего поворотом "головы"
        {
            eventData.pointerId = 1;
        }
    }
        
       
    private void CanTakeItem() //Может ли игрок взять предмет
    {
        Ray ray = new Ray(_Head.transform.position, _Head.transform.forward);
        float maxDistance = 1.8f;       
        if (Physics.Raycast(ray,out hit,maxDistance))
        {            
            float distance = Vector3.Distance(_Player.transform.position, hit.collider.gameObject.transform.position);          
            if ((hit.collider.gameObject.CompareTag("Item")&& distance<= maxDistance && !_Have_Item) || (hit.collider.gameObject.name == "Door" && distance <= 1))
            {
                _Can_Take = true;                           
            }
            else
            {
                _Can_Take = false;
            }
                 
        }
        else
        {
            _Can_Take = false;
        }
    }
    public void TakeItem() //Игрок берет предмет
    {
        if (!_Have_Item && hit.collider.gameObject.CompareTag("Item"))
        {          
            _Item_In_Hand = Instantiate(hit.collider.gameObject);
            _Item_In_Hand.transform.parent = GameObject.Find("Arm").transform;
            _Item_In_Hand.name = hit.collider.name;
            _Item_In_Hand.transform.localPosition = new Vector3(-0.4f, 0, -0.25f);
            _Item_In_Hand.transform.localEulerAngles = new Vector3(-90, 0, 0);
            _Item_In_Hand.GetComponent<Rigidbody>().isKinematic = true;
            _Item_In_Hand.GetComponent<MeshCollider>().isTrigger = true;
            Destroy(hit.collider.gameObject);
            _Have_Item = true;
        }
        
    }
    public void DropItem() //Игрок бросает предмет
    {
        if (_Have_Item)
        {         
            _Have_Item = false;
            _Item_In_Hand.GetComponent<Rigidbody>().isKinematic = false;
            _Item_In_Hand.GetComponent<MeshCollider>().isTrigger = false;
            _Item_In_Hand.GetComponent<Rigidbody>().AddForce(_Head.transform.forward*3, ForceMode.Impulse);
            _Item_In_Hand.transform.parent = null;

        }
    }
    public void SitDown() //Переход в положение "сидя"
    {
        if (!_Down)
        {
           CapsuleCollider character = _Player.GetComponent<CapsuleCollider>();
            character.height = 1.8f;
            _Head.transform.localPosition = new Vector3(0, 0.8f, 0);
        }
        else
        {
            CapsuleCollider character = _Player.GetComponent<CapsuleCollider>();
            character.height = 1;
            _Head.transform.localPosition = new Vector3(0, 0.4f, 0);
        }   
    }
    public void OpenDoor()
    {
        if(hit.collider.gameObject.name == "Door")
        {
            Door doorIsOpen = hit.collider.gameObject.GetComponent<Door>();           
                doorIsOpen.DoorIsOpen();
        }       
    }
    
}
