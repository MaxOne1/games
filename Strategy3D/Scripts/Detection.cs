﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detection : MonoBehaviour
{
    [SerializeField] private GameObject _Current_Enemy;

    private void FixedUpdate()
    {     
           if (_Current_Enemy == null)
            {
                transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Run", false);
                transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Attack", false);
            }      
    }  
    private void OnTriggerEnter(Collider other)
    {
        if (transform.parent.gameObject.CompareTag("Warrior"))
        {
            if (other.CompareTag("Enemy"))
            {
                _Current_Enemy = other.gameObject;               
            }
        }
        if (transform.parent.gameObject.CompareTag("Enemy"))
        {
            if (other.CompareTag("Warrior"))
            {
                _Current_Enemy = other.gameObject;
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {       
        if (transform.parent.gameObject.CompareTag("Warrior"))
        {
            if (other.CompareTag("Enemy"))
            {
                transform.parent.gameObject.GetComponent<Warriors>().Hunting(other.gameObject);
                
            }
            if (other.gameObject == null)
            {
                    _Current_Enemy = null;
                    transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Run", false);
                    transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Attack", false);
            }
            
        }
        if (transform.parent.gameObject.CompareTag("Enemy"))
        {
            if (other.CompareTag("Warrior"))
            {
                transform.parent.gameObject.GetComponent<Warriors>().Hunting(other.gameObject);
                if (Vector3.Distance(transform.parent.transform.position, transform.parent.GetComponent<Enemy>()._Start_Position) > transform.parent.GetComponent<Enemy>()._Border)
                {
                    _Current_Enemy = null;
                    transform.parent.GetComponent<Enemy>()._Agent.SetDestination(transform.parent.GetComponent<Enemy>()._Start_Position);
                }
            }
            if (other.gameObject == null)
            {
                _Current_Enemy = null;
                transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Run", false);
                transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Attack", false);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (transform.parent.gameObject.CompareTag("Warrior"))
        {
            if (other.CompareTag("Enemy"))
            {
                _Current_Enemy = null;
                transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Run", false);
                transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Attack", false);

            }
        }
        if (transform.parent.gameObject.CompareTag("Enemy"))
        {
            if (other.CompareTag("Warrior"))
            {
                _Current_Enemy = null;
                transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Run", false);
                transform.parent.gameObject.GetComponent<Warriors>()._Animator.SetBool("Attack", false);

            }
            if (Vector3.Distance(transform.parent.transform.position, transform.parent.GetComponent<Enemy>()._Start_Position) > transform.parent.GetComponent<Enemy>()._Border)
            {
                _Current_Enemy = null;
                transform.parent.GetComponent<Enemy>()._Agent.SetDestination(transform.parent.GetComponent<Enemy>()._Start_Position);
            }
        }
    }

}
