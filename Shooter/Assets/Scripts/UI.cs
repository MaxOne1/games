using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    [SerializeField] private Image _Health_Player;
    [SerializeField] private GameObject _Panel;
    [SerializeField] private Text _Ammunition_Count_Text;

    public void PlayerHealth(int _health)
    {
        _Health_Player.fillAmount = _health / 100f;
    }
    public void GameOver()
    {
        Time.timeScale = 0;
        _Panel.SetActive(true);
    }
    public void Repeat()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    public void AmmunitionCount(int _ammunition)
    {
        _Ammunition_Count_Text.text = "" + _ammunition;
    }
   
}
