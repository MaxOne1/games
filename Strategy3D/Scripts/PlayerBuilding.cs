﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBuilding : MonoBehaviour
{
    [SerializeField] private float _Create_Time;
    [SerializeField] private GameObject _Warrior_Spear;
    [SerializeField] private GameObject _Warrior_Bow;
    [SerializeField] private List<GameObject> _Warriors;
    [SerializeField] private Transform _Respawn_Point;
    [SerializeField] private Image _Time_Scale;
    [SerializeField] private InputField _Input_Count;
    private GameObject _Current_Warrioir;
    private string _Count_Text;
   
    private IEnumerator CreateWarriors()
    {       
        while (_Warriors.Count > 0)
        {
            TimeBar();
            yield return new WaitForSeconds(_Create_Time);
            Instantiate(_Warriors[0], _Respawn_Point.position, Quaternion.identity);
            _Time_Scale.fillAmount = 0;
            _Warriors.Remove(_Warriors[0]);
        }          
       
    }
    private void Update()
    {
        TimeBar();
    }
    public void Spearmans()
    {
        _Current_Warrioir = _Warrior_Spear;
    }
    public void Archers()
    {
        _Current_Warrioir = _Warrior_Bow;
    }
    public void TimeBar()
    {
        if (_Warriors.Count >0)
        {
            _Time_Scale.fillAmount += (Time.deltaTime / _Create_Time);
        }      
    }  
    public void StartCreate(int _count)
    {       
            _Count_Text = _Input_Count.text;
            _count = int.Parse(_Count_Text);
            for (int i = 0; i < _count; i++)
            {
                _Warriors.Add(_Current_Warrioir);
            }
            StartCoroutine(CreateWarriors());                   
    }
}
