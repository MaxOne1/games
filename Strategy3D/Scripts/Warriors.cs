﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Warriors : MonoBehaviour
{      
    [SerializeField] protected GameObject _Current_Enemy;
    public float _Damage;
    public float _Health;
    [Range(0,1)]public float _Armor;
    
    public Animator _Animator;
    public NavMeshAgent _Agent;
    protected void Start()
    {
        _Agent = GetComponent<NavMeshAgent>();
        _Animator = GetComponent<Animator>();
        _Animator.SetBool("Run", false);
    }
    private void FixedUpdate()
    {
        if (_Health <= 0)
        {
            _Animator.SetBool("Die", true);
            Destroy(gameObject, 3);
        }       
    }
   
    public virtual void Hunting(GameObject _enemy)
    {
        if (_enemy != null)
        {
            _Current_Enemy = _enemy;
            _Animator.SetBool("Run", true);
            _Agent.SetDestination(_enemy.transform.position);
            transform.LookAt(_enemy.transform.position);
            if (Vector3.Distance(transform.position, _enemy.transform.position) <= _Agent.stoppingDistance)
            {               
                if (_Health > 0)
                {
                    _Animator.SetBool("Attack", true);
                }
            }
            else
            {
                _Animator.SetBool("Attack", false);
            }
        }
        else
        {
            _Animator.SetBool("Run", false);
            _Animator.SetBool("Attack", false);
        }

    }
    protected void Damage()
    {

        _Current_Enemy.GetComponent<Warriors>()._Health -= (_Damage * (1- _Current_Enemy.GetComponent<Warriors>()._Armor));
    }
}
