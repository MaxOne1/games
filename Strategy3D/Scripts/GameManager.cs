﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    [SerializeField] private LayerMask _Game_Layer;
    [SerializeField] private GameObject _Building_Window;
    [SerializeField] private GameObject _Victory_Panel;
   
    public void Click()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 500))
        {
            if (hit.collider.CompareTag("Warrior"))
            {
                if (!hit.collider.GetComponent<PlayerSoldier>()._Is_Clicked)
                {
                    hit.collider.GetComponent<PlayerSoldier>()._Is_Clicked = true;
                }
                else
                {
                    hit.collider.GetComponent<PlayerSoldier>()._Is_Clicked = false;
                }                
            }          
            if (hit.collider.CompareTag("Building"))
            {
                _Building_Window.SetActive(true);
            }
            else
            {
                _Building_Window.SetActive(false);
            }
        }     
    }
    private void HaveEnemies()
    {
         GameObject[] _enemy = GameObject.FindGameObjectsWithTag("Enemy");
        if(_enemy.Length == 0)
        {
            _Victory_Panel.SetActive(true);
            Time.timeScale = 0;
        }
    }
    private void FixedUpdate()
    {
        HaveEnemies();
    }

}
