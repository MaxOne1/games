﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameOver : MonoBehaviour
{
    public GameObject panel;
    private FirstPersonController playerController;

    public void Start()
    {
        playerController = GameObject.Find("Player").GetComponent<FirstPersonController>();
        
    }
    private void Update()
    {
       
    }
    public void Lose()
    {

        panel.SetActive(true);
        Time.timeScale = 0;         
    }
}
