﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIGame : MonoBehaviour
{
    [SerializeField] private GameObject _VictoryPanel;
    [SerializeField] private GameObject _DefeatPanel;
    [SerializeField] private GameObject _Main_Menu;
    [SerializeField] private Text _Moves_Text;
    [SerializeField] private Text _Moves_Score_Text;
    [SerializeField] private Text _Moves_Number_Text;
    private int _Moves;
    
    private void Start()
    {
        _Moves = PlayerPrefs.GetInt("Moves");
        Score();
    }
    public void Victory(int _moves)
    {
        _VictoryPanel.SetActive(true);
        _Moves_Number_Text.text = "" + _moves;
    }
    public void Defeat()
    {
        _DefeatPanel.SetActive(true);
    }
    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
    public void Moves(int _moves)
    {
        _Moves_Text.text = "Moves: " + _moves;
    }
    public void ClassicMode()
    {
        Game _game_Script = GameObject.Find("GameManager").GetComponent<Game>();
        _game_Script.ClassicMode();
        _Main_Menu.SetActive(false);
    }
    public void NormalMode()
    {
        Game _game_Script = GameObject.Find("GameManager").GetComponent<Game>();
        _game_Script.NormalMode();
        _Main_Menu.SetActive(false);
    }
    private void Score()
    {
        _Moves_Score_Text.text = "" + _Moves;
    }
}
