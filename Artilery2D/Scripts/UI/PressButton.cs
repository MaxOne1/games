﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PressButton : MonoBehaviour, IPointerDownHandler,IPointerUpHandler
{
    public bool fireButtonDrag;
    public bool dragBazooka;

    void Start()
    {
       
    }
    public void OnPointerDown(PointerEventData eventData)
    {
       
        fireButtonDrag = true;
        dragBazooka = false;

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        
        fireButtonDrag = false;
        dragBazooka = true;

    }
   
   
    
}

