﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShakeAdButton : MonoBehaviour
{
    void Start()
    {
        InvokeRepeating("Shake", 1, 5);
    }

    // Update is called once per frame
    void Update()
    {
      
    }
    void Shake()
    {
        transform.DOShakePosition(1,8);
    }
}
