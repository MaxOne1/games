using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    private PlayerWeapon _Weapon;
    private UI _UI_Script;

    private void Start()
    {
        _Weapon = GetComponent<PlayerWeapon>();
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
    }
    private void Update()
    {
        Shooting();
    }
    public void Shooting()
    {
        if (Input.GetMouseButton(0) && _Weapon._Current_Weapon !=null)
        {
            _Weapon._Gun_Script.Shoot();
            _UI_Script.AmmunitionCount(_Weapon._Current_Weapon.GetComponent<Gun>()._Ammunition);
        }
    }

}
