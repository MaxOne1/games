﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionObject : MonoBehaviour
{
    [SerializeField] private ParticleSystem _Explosion;
    [SerializeField] private AudioClip _Explosion_Sound;
    private AudioSource _Audio_Source;
    void Start()
    {
        _Audio_Source = GetComponent<AudioSource>();
        ExplosionEffect();
    }

    private void ExplosionEffect()
    {
       _Explosion.Play();
       _Audio_Source.PlayOneShot(_Explosion_Sound);
       Destroy(gameObject, _Explosion_Sound.length);
    }
    
}
