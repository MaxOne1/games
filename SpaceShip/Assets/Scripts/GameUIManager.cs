﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameUIManager : MonoBehaviour
{
    public List<Image> _Health_Points;
    public Image _Health_Point;
    [SerializeField] private Texture2D _Cursor;
    [SerializeField] private TextMeshProUGUI _Score_Text;
    [SerializeField] private TextMeshProUGUI _Final_Score_Text;
    [SerializeField] private GameObject _DefeatPanel;
    private GameManager _Manager_Script;
    public int _Health_Index;
    private void Start()
    {      
        _Manager_Script = GameObject.Find("GameManager").GetComponent<GameManager>();
        Cursor.SetCursor(_Cursor, Vector2.zero, CursorMode.ForceSoftware);
        _Score_Text.text = "" + _Manager_Script._Score;
        _Health_Index = 0;
    }
     
    public void HealthPoints()
    {        
        if (_Health_Index < _Health_Points.Count)
        {
            _Health_Points[_Health_Index].enabled = false;
            _Health_Index++;
        }                     
    }  
    public void ScoreText()
    {
        _Score_Text.text = "" + _Manager_Script._Score;       
    }
    public void Pause()
    {
        Time.timeScale = 0;
    }
    public void Continue()
    {
        Time.timeScale = 1;
    }
    public void Defeat()
    {
        _DefeatPanel.SetActive(true);
        _Final_Score_Text.text = "" + _Manager_Script._Score;
    }
    public void Menu()
    {
        SceneManager.LoadScene(0);
    }
}
