﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ad : MonoBehaviour
{
    public Controller playerScript;  
    void Start()
    {
        IronSource.Agent.init("faed1fc1", IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL, IronSourceAdUnits.OFFERWALL, IronSourceAdUnits.BANNER);
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
        //IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
        IronSource.Agent.shouldTrackNetworkState(true);
    }
    
   
    public void InintBanner()
    {
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.TOP);

    }
    
    public void LoadFullScreen()
    {
        IronSource.Agent.loadInterstitial();
    }
    public void ShowFullScreen()
    {
        IronSource.Agent.showInterstitial();
    }
    public void ShowReward()
    {
        IronSource.Agent.showRewardedVideo("Game_Over");
    }   
    void RewardedVideoAdOpenedEvent()
    {
    }
    void RewardedVideoAdClosedEvent()
    {

    }
    public void RewardedVideoAvailabilityChangedEvent(bool available)
    {
       
        bool rewardedVideoAvailability = available;
       

    }
    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        playerScript.Reward();
        placement.getPlacementName();
        placement.getRewardName();
        placement.getRewardAmount();
    }
   
    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
    }
    void RewardedVideoAdStartedEvent()
    {
    }
 
    void RewardedVideoAdEndedEvent()
    {
    }

    void RewardedVideoAdClickedEvent()
    {
    }
   

}
