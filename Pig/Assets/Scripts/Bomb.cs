﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    
    [SerializeField] float _Explosion_Radius;
    [SerializeField] protected float _Damage;    
    [SerializeField] protected GameObject _Explosion_Object;
    private SpriteRenderer _Sprite_Renderer;      
    void Start()
    {
        _Sprite_Renderer = GetComponent<SpriteRenderer>();
        StartCoroutine(Explosion());
    }

   
    IEnumerator Explosion()
    {
        InvokeRepeating("DangerColor", 1,1);
        InvokeRepeating("DefaultColor", 0,2);
        yield return new WaitForSeconds(5);
        Instantiate(_Explosion_Object, transform.position, Quaternion.identity);
        ToDamage();
        Destroy(gameObject);
    }
    void ToDamage()
    {
        Collider2D[] _affected_Subjects = Physics2D.OverlapCircleAll(transform.position, _Explosion_Radius);
        foreach(Collider2D _subject in _affected_Subjects)
        {
            if (_subject.gameObject.CompareTag("Farmer"))
            {
                _subject.GetComponent<FarmerMovement>().TakeDamage(_Damage);
            }
            if (_subject.gameObject.CompareTag("Dog"))
            {
                _subject.GetComponent<DogMovement>().GoSleep();
            }
        }

    }
    private void DefaultColor()
    {
        _Sprite_Renderer.color = Color.white;
    }
    private void DangerColor()
    {
        _Sprite_Renderer.color = Color.red;
    }

}
