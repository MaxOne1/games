﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryObject : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip win;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(win);
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject,win.length);
    }
}
