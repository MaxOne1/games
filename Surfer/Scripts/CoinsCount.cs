﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsCount : MonoBehaviour
{
    public Text _Coins_Count;
    public int _Count;

    private void Start()
    {
        _Count = 0;
    }
    private void Update()
    {
        _Coins_Count.text = "" + _Count.ToString();
    }
}
