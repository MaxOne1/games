﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using DG.Tweening;
using TMPro;

public class UI : MonoBehaviour
{
    [SerializeField]protected bool gameOver;
    [SerializeField] private RectTransform endPanel;
    [SerializeField] private GameObject darkPanel;
    [SerializeField] TextMeshProUGUI win;
    [SerializeField] TextMeshProUGUI loose;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private AudioClip music;
    private AudioSource aSource;
    void Start()
    {
        gameOver = false;
        aSource = GetComponent<AudioSource>();
        darkPanel.SetActive(false);
        pauseMenu.SetActive(false);
        aSource.Play();
        Time.timeScale = 1;
    }
    public void EndPanel()
    {
        gameOver = true;
        endPanel.DOAnchorPos(new Vector2(7, -20), 3.5f);
        darkPanel.SetActive(true);
        win.enabled = false;
        loose.enabled = true;
    }
    public void Repeat()
    {
        StartCoroutine(Reload());      
    }
    IEnumerator Reload()
    {
        endPanel.DOAnchorPos(new Vector2(7, 2500), 2f);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(0);
    }
    public void WinPanel()
    {
        gameOver = true;
        endPanel.DOAnchorPos(new Vector2(7, -20), 3.5f);
        darkPanel.SetActive(true);
        win.enabled = true;
        loose.enabled = false;
    }
    public void Resume()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        aSource.Play();
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        aSource.Pause();
    }


}
