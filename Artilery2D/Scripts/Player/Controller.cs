﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class Controller : MonoBehaviour


{
    public Transform player;

    private Vector3 mousePos;

    public float speedX;   
    public float jumpForce;
    private float beginSpeed;

    public UnityEngine.UI.Image HPBar;
    public GameObject forceBar;

    public float health;

    private Rigidbody2D rb;

    public SpriteRenderer flip;
    
    public GameObject[] weapons;

    private Vector2 origPos;
    
    private bool forwardDirection;
    public bool isGround;  
   
    private TimeManage timeScript;

    private Animator walk;

    private PressButton fireButton;
    public bool aimActive;

    void Start()
    {
        beginSpeed = speedX;
        origPos = transform.position;
        rb = GetComponent<Rigidbody2D>();
        flip = GetComponent<SpriteRenderer>();
        timeScript = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManage>();
        fireButton = GameObject.FindGameObjectWithTag("FireButton").GetComponent<PressButton>();
        walk = GetComponent<Animator>();
    }
    private void OnGUI()
    {
        Event ev = Event.current;
        if (ev.isMouse && timeScript.playerMove && !timeScript.gameOver)
        {
            if(ev.clickCount == 2 )
            {
                Jump();
                Debug.Log("Jump!");
            }
        }
            
    }

    void Update()
    {
        if (!timeScript.gameOver)
        {
            Moving();
            Die();
            Weapons();
            StopMove();
            HpBar();
            Borders();
            ForceBarPosition();         
        }      
        
       
    }
    void HpBar()
    {
        HPBar.fillAmount = health / 100;
        if (HPBar.fillAmount <= 0)
            timeScript.gameOver = true;

        HPBar.transform.position = player.position + new Vector3(0, 3.5f, 0);       
    }
    void ForceBarPosition()
    {
        forceBar.transform.position = player.position + new Vector3(0 ,4.4f, 0);
    }
      
    public void MoveLeft()
    {
        if (Input.GetMouseButton(0))
        {
            transform.Translate(Vector2.left * -speedX * Time.deltaTime);
            forwardDirection = false;
            transform.rotation = Quaternion.Euler(transform.rotation.x, 180, transform.rotation.z);
            walk.SetBool("move", true);
        }
            
        else
            rb.velocity = new Vector2(0, 0);
            walk.SetBool("move", false);
    }

    public void MoveRight()
    {
        if (Input.GetMouseButton(0))
        {
            transform.Translate(Vector2.right * speedX * Time.deltaTime);
            forwardDirection = true;
            transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
            walk.SetBool("move", true);
        }
            
        else
            rb.velocity = new Vector2(0, 0);
        walk.SetBool("move", false);
    }
    public void Moving()
    {
        mousePos = Input.mousePosition;
        Vector2 camPos = Camera.main.ScreenToWorldPoint(mousePos);
        if (camPos.x < player.position.x &&  timeScript.playerMove && !fireButton.fireButtonDrag)
        {
            MoveLeft();

        }
        if (camPos.x > player.position.x &&  timeScript.playerMove && !fireButton.fireButtonDrag)
        {
            MoveRight();

        }       

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Ground")|| collision.collider.CompareTag("Obstacle")|| collision.collider.CompareTag("Enemy"))
        {
            isGround = true;
        }       
    }

    public void Jump()
    {   
        if(isGround == true)
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            isGround = false;
        }

        
       
    }
    void StopMove()
    {
        if (fireButton.fireButtonDrag)
            speedX = 0;
        if (!fireButton.fireButtonDrag)
            speedX = beginSpeed;
    }
    private void OnMouseDrag()
    {
        aimActive = true;
        if (timeScript.playerMove && !timeScript.gameOver)
        {
            //aim.SetActive(true);
            mousePos = Input.mousePosition;
            Vector2 camPos = Camera.main.ScreenToWorldPoint(mousePos);
            Vector2 difference = Camera.main.ScreenToWorldPoint(mousePos) - transform.position;
            float rotateZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            Vector2 distanceToAim = new Vector2(camPos.x + 5, camPos.y);

            foreach (GameObject weapon in weapons)
            {
                if (camPos.x < player.position.x)
                {
                    weapon.transform.rotation = Quaternion.Euler(180, 0, -rotateZ);
                }
                else
                {
                    weapon.transform.rotation = Quaternion.Euler(0, 0, rotateZ);
                }                    
            }
            speedX = 0;
           
        }
    }
    private void OnMouseUp()
    {
        aimActive = false;
        if(timeScript.playerMove &&!timeScript.gameOver)
        speedX = beginSpeed;
    }
    public void Reward()
    {
        timeScript.gameOver = false;
        transform.position = origPos;
        health = 50;
        timeScript.timeMove = 30;
        timeScript.playerMove = true;
        timeScript.enemyMove = false;
        timeScript.gameOverPanel.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            health -= collision.GetComponent<Bullet>().damage;
            Destroy(collision.gameObject);
        }
    }
    void Die()
    {
        if (health <= 0)
        {
            Debug.Log("YOU LOSE");
            TimeManage gameOver = GameObject.Find("TimeManager").GetComponent<TimeManage>();
            gameOver.gameOver = true;
        }
       
            
    }
    void Borders()
    {
        if (transform.position.y < 115 || transform.position.y > 150 || transform.position.x > 25 || transform.position.x < -25)
        {
            health = 0;
        }
    }
    void Weapons()
    {
        UIManager uiManage;
        uiManage = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        if (uiManage.riffle)
        {
            weapons[0].SetActive(true);
            weapons[1].SetActive(false);
            weapons[2].SetActive(false);
            weapons[3].SetActive(false);
            fireButton.dragBazooka = false;
            forceBar.SetActive(false);
        }

        if (uiManage.shotGun)
        {
            weapons[0].SetActive(false);
            weapons[1].SetActive(true);
            weapons[2].SetActive(false);
            weapons[3].SetActive(false);
            fireButton.dragBazooka = false;
            forceBar.SetActive(false);
        }
        if (uiManage.sniperRifle)
        {
            weapons[0].SetActive(false);
            weapons[1].SetActive(false);
            weapons[2].SetActive(true);
            weapons[3].SetActive(false);
            fireButton.dragBazooka = false;
            forceBar.SetActive(false);

        }
        if (uiManage.bazooka)
        {
            weapons[0].SetActive(false);
            weapons[1].SetActive(false);
            weapons[2].SetActive(false);
            weapons[3].SetActive(true);
            forceBar.SetActive(true);

        }
    }
    
}
