﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsManager : MonoBehaviour
{
    public List<GameObject> items = new List<GameObject>();
    private List<int> randomItem = new List<int>();
    public List<Vector3> places = new List<Vector3>();
    private List<int> randomPlace = new List<int>();
    void Start()
    { 
        for (;randomItem.Count < items.Count;)
        {
            int indexItem = Random.Range(0, items.Count);
            int indexPlace = Random.Range(0, places.Count);
            if (!randomItem.Contains(indexItem) && !randomPlace.Contains(indexPlace))
            {
                randomItem.Add(indexItem);
                randomPlace.Add(indexPlace);
                Instantiate(items[indexItem],places[indexPlace], Quaternion.identity);
            }
        }
        randomItem.Clear();
        randomPlace.Clear();
    }
   

    

}
