using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Gun
{
    [SerializeField] private float _Bullet_Spread;

    protected override void BulletSpread(float _bullet_spread)
    {
        base.BulletSpread(_Bullet_Spread);
    }
}
