﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Flashlight : MonoBehaviour
{
    public GameObject LightOnOff;
    public bool LightWork;
    void Start()
    {
        LightOnOff.SetActive(false);
        LightWork = false;
    }

   
    public void FlashlightOnOff()
    {
        if (LightWork == true)
        {
            LightWork = false;
            LightOnOff.SetActive(false);
        }
        else
        {
            LightWork = true;
            LightOnOff.SetActive(true);
        }
    }


}
