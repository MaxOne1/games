﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private float _Speed_Rotate;
    private CoinsCount _Count_Script;
    void Update()
    {
        transform.Rotate(Vector3.forward * _Speed_Rotate * Time.deltaTime);
        _Count_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<CoinsCount>();
    }   
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Cube"))
        {
            _Count_Script._Count += 1;
            GameObject _coin = GameObject.FindGameObjectWithTag("CoinsImage");
            _coin.GetComponent<Animator>().SetTrigger("CoinsUp");         
            Destroy(gameObject);
        }
    }
}
