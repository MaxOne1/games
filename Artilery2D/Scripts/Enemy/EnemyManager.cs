﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject[] enemies;
    public GameObject winPanel;  
   
    void Start()
    {
        winPanel.SetActive(false);
       
    }

    // Update is called once per frame
    void Update()
    {
        
        for(int i = 0; i < enemies.Length; i++)
        {
            if (enemies[i] == null)
            {
                winPanel.SetActive(true);
                Time.timeScale = 0;
            }
        }
    }
}
