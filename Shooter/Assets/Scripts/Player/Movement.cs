using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float _Speed_Move;
    [SerializeField] private float _Speed_Rotate;
    [SerializeField] private GameObject _Head;
    [SerializeField] private GameObject _Player;
    private float _Move_H;
    private float _Move_V;
    private float _Rotate_X = 0;
    private float _Rotate_Y = 0;
    
    void Update()
    {
        Move();
        Rotation();
    }
    private  void Move()
    {
        _Move_H = Input.GetAxis("Horizontal");
        _Move_V = Input.GetAxis("Vertical");
        Vector3 _move = new Vector3(_Move_H, 0, _Move_V);
        transform.Translate(_move * _Speed_Move * Time.deltaTime);
    }
    private void Rotation()
    {
        _Rotate_X += Input.GetAxis("Mouse X") * _Speed_Rotate * Time.deltaTime;
        _Rotate_Y -= Input.GetAxis("Mouse Y") * _Speed_Rotate * Time.deltaTime;
        _Rotate_Y = Mathf.Clamp(_Rotate_Y, -90, 90);
        _Head.transform.localRotation = Quaternion.Euler(_Rotate_Y, 0, 0);
        _Player.transform.localRotation = Quaternion.Euler(0, _Rotate_X, 0);                          
    }
}
