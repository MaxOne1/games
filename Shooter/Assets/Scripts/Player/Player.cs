using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Characteristics
{
    private UI _UI_Script;

    private void Start()
    {
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            _UI_Script.PlayerHealth(_Health);
            if(_Health <= 0)
            {
                _UI_Script.GameOver();
            }
        }
    }
}
