﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] Sprite up;
    [SerializeField] Sprite down;
    [SerializeField] Sprite right;
    [SerializeField] Sprite left;
    [SerializeField] GameObject bomb;   
    [SerializeField] private TextMeshProUGUI bombCountText;
    [SerializeField] private AudioClip feet;
    private int bombCount;
    private Rigidbody2D rb;
    private AudioSource aSource;   
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        bombCount = 0;
        bombCountText.text = "Bomb Count: " + bombCount;
        aSource = GetComponent<AudioSource>();
    }

     
    public void MoveUp()
    {
        rb.velocity = new Vector2(0, speed);
            this.GetComponent<SpriteRenderer>().sprite = up;

    }
    public void MoveDown()
    {

        rb.velocity = new Vector2(0, -speed);
        this.GetComponent<SpriteRenderer>().sprite = down;
    }   

    public void MoveRight()
    {

        rb.velocity = new Vector2(speed, 0);
        this.GetComponent<SpriteRenderer>().sprite = right;
        
    }
    public void MoveLeft()
    {

        rb.velocity = new Vector2(-speed, 0);
        this.GetComponent<SpriteRenderer>().sprite = left;
        
    }
    public void Stop()
    {
        rb.velocity = new Vector2(0, 0);
    }
    public void Bomb()
    {
        if(bombCount != 0)
        {
            Instantiate(bomb, transform.position, Quaternion.identity);
            bombCount--;
            bombCountText.text = "Bomb Count: " + bombCount;
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("BombPrefab"))
        {
            Destroy(collision.gameObject);
            bombCount++;
            bombCountText.text = "Bomb Count: " + bombCount;
        }
    }

    public void PlaySound()
    {
        aSource.Play();
    }

    public void StopSound()
    {
        aSource.Stop();
    }
}

