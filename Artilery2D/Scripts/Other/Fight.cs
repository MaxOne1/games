﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fight : MonoBehaviour
{
    public AudioClip fight;
    private AudioSource aSource;
    void Start()
    {
        aSource = GetComponent<AudioSource>();
        aSource.PlayOneShot(fight);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
