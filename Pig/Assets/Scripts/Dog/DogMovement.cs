﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogMovement : MonoBehaviour
{
    [SerializeField] private Transform[] _Target_Position;
    [SerializeField] private int _Index;
    [SerializeField] private float _Speed;
    [SerializeField] private float _Sleep_Time;
    private bool _Is_Sleep;
    [SerializeField] private ParticleSystem _Sleep;
    private GameManager _Manager_Script;
    private UI _UI_Script;
    private DogImage _Dog_Image_Script;
    void Start()
    {
        _Manager_Script = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
        _Dog_Image_Script = GetComponent<DogImage>();
        _Index = 0;
        _Is_Sleep = false;
    }

    // Update is called once per frame
    private void Update()
    {      
        Move();
    }

    private void Move()
    {
        if (!_Is_Sleep)
        {
            transform.position = Vector2.MoveTowards(transform.position, _Target_Position[_Index].position, _Speed * Time.deltaTime);
            _Dog_Image_Script.ChangeDiretion(_Target_Position, _Index);
            if (Vector2.Distance(transform.position, _Target_Position[_Index].position) <= 0f)
            {
                _Index++;
                if (_Index == _Target_Position.Length)
                {
                    _Index = 0;
                }
            }
        }          
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && !_Is_Sleep)
        {
            _Manager_Script.StopGame();
            _UI_Script.PlayerDefeat();
        }
    }
    public void GoSleep()
    {
        StartCoroutine(Sleep());
    }
    private IEnumerator Sleep()
    {
        _Is_Sleep = true;
        _Sleep.Play();
        yield return new WaitForSeconds(_Sleep_Time);
        _Sleep.Stop();
        _Is_Sleep = false;
    }
}
