﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownImage : MonoBehaviour
{
    [SerializeField] Sprite _Down_Image;
    public void MoveDown()
    {
        this.GetComponent<SpriteRenderer>().sprite = _Down_Image;
    }
}
