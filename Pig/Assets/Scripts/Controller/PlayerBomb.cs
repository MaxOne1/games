﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBomb : MonoBehaviour
{
    [SerializeField] GameObject _Bomb;
    private UI _UI_Manager;
    private int _BombCount;

    private void Start()
    {
        _UI_Manager = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
        _BombCount = 0;
    }
    public void PutBomb()
    {
        if (_BombCount != 0)
        {
            Instantiate(_Bomb, transform.position, Quaternion.identity);
            _BombCount--;
            _UI_Manager.BombCountText(_BombCount);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("BombPrefab"))
        {
            BombAdd(collision.gameObject);
        }
    }

    private void BombAdd(GameObject _bomb)
    {
        Destroy(_bomb);
        _BombCount++;
        _UI_Manager.BombCountText(_BombCount);
    }
}
