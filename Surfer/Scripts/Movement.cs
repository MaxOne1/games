﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float _Speed;
    void Update()
    {
        transform.Translate(Vector3.forward * _Speed * Time.deltaTime);
    }
   
    
}
