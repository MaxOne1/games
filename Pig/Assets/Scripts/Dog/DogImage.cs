﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogImage : MonoBehaviour
{
    [SerializeField] Sprite _Up;
    [SerializeField] Sprite _Down;
    [SerializeField] Sprite _Right;
    [SerializeField] Sprite _Left;

    public void ChangeDiretion(Transform[] _target_Position, int _index)
    {
        float _difference_X = Mathf.Abs(transform.position.x - _target_Position[_index].position.x);      
        float _difference_Y = Mathf.Abs(transform.position.y - _target_Position[_index].position.y);      
        if (transform.position.x < _target_Position[_index].transform.position.x && _difference_X > _difference_Y)
        {
            this.GetComponent<SpriteRenderer>().sprite = _Right;
        }
        if (transform.position.x > _target_Position[_index].transform.position.x && _difference_X > _difference_Y)
        {
            this.GetComponent<SpriteRenderer>().sprite = _Left;
        }
        if (transform.position.y < _target_Position[_index].transform.position.y && _difference_X < _difference_Y)
        {
            this.GetComponent<SpriteRenderer>().sprite = _Up;
        }
        if (transform.position.y > _target_Position[_index].transform.position.y && _difference_X < _difference_Y)
        {
            this.GetComponent<SpriteRenderer>().sprite = _Down;
        }
    }
}
