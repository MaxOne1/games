﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] private ParticleSystem _Explosion;
    private void Start()
    {
        _Explosion.Play();
    }
    private void Update()
    {
        Destroy(gameObject, 0.25f);
    }


}
