﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using TMPro;

public class TimeManager : MonoBehaviour
{
    private float timeSeconds;
    private int timeMinutes;
    private string stringSeconds;
    private string stringMinutes;
    private FirstPersonController playerController;
    public TextMeshProUGUI gameTime;

    void Start()
    {
        playerController = GameObject.Find("Player").GetComponent<FirstPersonController>();
        timeMinutes = 0;
        timeSeconds = 0.0f;
    }
  

    // Update is called once per frame
    void Update()
    {
        timeSeconds += Time.deltaTime;
        stringSeconds = timeSeconds.ToString();
        stringMinutes = timeMinutes.ToString();
        gameTime.text = "Time "+stringMinutes+":" + stringSeconds;
        if(timeSeconds >= 60.0f)
        {
            timeMinutes++;
            timeSeconds = 0.0f;
        }
       
        
    }
}
