﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{   
    [SerializeField] private int _Points;
    [SerializeField] private GameObject _Particle_Object;
    private float _Current_Speed;
    private Vector2 _Direction;
    private float _Direction_X;
    private float _Direction_Y;
    private Rigidbody2D _Rigidbody;
    private GameManager _Manager_Script;
    private GameUIManager _UI_Script;
    private void Start()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Manager_Script = GameObject.Find("GameManager").GetComponent<GameManager>();
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<GameUIManager>();
        _Direction_X = Random.Range(-1f, 1f);
        _Direction_Y = Random.Range(-1f, 1f);
        _Direction = new Vector2(_Direction_X, _Direction_Y);      
        _Current_Speed = _Manager_Script._Asteroids_Speed;      
    }

    private void FixedUpdate()
    {
        Movement();
        _Manager_Script.Border("Asteroid",this.gameObject);
    }  
    private void Movement()
    {
        _Rigidbody.velocity = _Direction * _Current_Speed * Time.deltaTime;
        _Current_Speed += (_Current_Speed*0.01f) / 60f;
        _Manager_Script._Asteroids_Speed = _Current_Speed;
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Asteroid"))
        {
            _Direction = new Vector2(-_Direction_X, -_Direction_Y);
        }
    }
   
    public void Death()
    {
        _Manager_Script.Score(_Points);
        _UI_Script.ScoreText();
        Instantiate(_Particle_Object, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
