﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponRotation : MonoBehaviour
{
    public Transform player;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rotation();
    }
    void Rotation()
    {
        if (player.position.x > transform.position.x)
        {
            var dir = player.position - transform.position;
            var euler = transform.eulerAngles;
            euler.z = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.eulerAngles = euler;

        }
        else
        {
            var dir = player.position - transform.position;
            var euler = transform.eulerAngles;
            euler.z = Mathf.Atan2(dir.y, -dir.x) * Mathf.Rad2Deg;
            transform.eulerAngles = euler;
        }
    }
}
