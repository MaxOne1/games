﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectStart : MonoBehaviour
{
    public ParticleSystem effect;
    public AudioClip boom;
    private AudioSource audioController;
    void Start()
    {
        effect.Play();
        audioController = GetComponent<AudioSource>();
        if(boom != null)
        audioController.PlayOneShot(boom,1);
    }


    void Update()
    {
        if (boom != null)
            Destroy(gameObject, boom.length);
        else
            Destroy(gameObject, 0.5f);
        
        
    }  
}
