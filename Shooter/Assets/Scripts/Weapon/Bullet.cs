using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float _Speed;
    [SerializeField] protected int _Damage;
    [SerializeField] private int _Fire_Range;
    private Vector3 _Start_Pos;

    private void Start()
    {
        _Start_Pos = transform.position;
    }
    private void Update()
    {
        transform.Translate(Vector3.forward * _Speed * Time.deltaTime);
        FireRange();
    }
   
    private void OnTriggerEnter(Collider other)
    {
        Damage(other.gameObject);
    }   
    private void FireRange()
    {
        float _current_pos = Vector3.Distance(_Start_Pos, transform.position);
        if(_current_pos > _Fire_Range)
        {
            Destroy(gameObject);
        }
    }
    protected virtual void Damage(GameObject _target)
    {
        Characteristics _character = _target.GetComponent<Characteristics>();
        if (_character != null)
        {
            _character.GetDamage(_Damage);
            Destroy(gameObject);
        }
    }
    
}
