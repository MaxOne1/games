﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimeManage : MonoBehaviour
{
    public float timeMove; 
    public bool playerMove;
    private string stringSeconds;
    public float setTime;
    public bool enemyMove;
    public TextMeshProUGUI timeText;
    public bool gameOver;
    public GameObject gameOverPanel;
    
    void Start()
    {
        timeMove = setTime;
        playerMove = true;
        gameOverPanel.SetActive(false);
        
    }

    
    void Update()
    {
        TimeControl();
        if (gameOver)
            gameOverPanel.SetActive(true);
        if (!gameOver)
            gameOverPanel.SetActive(false);
        ActiveButton();

    } 
    
    void TimeControl()
    {
        if (!gameOver)
        {
            timeMove -= Time.deltaTime;
            stringSeconds = timeMove.ToString("f0");
            timeText.text = "" + stringSeconds;
            if (timeMove <= 0)
            {
                if (playerMove)
                {
                    enemyMove = true;
                    playerMove = false;
                    timeMove = setTime;
                    PressButton fire = GameObject.FindGameObjectWithTag("FireButton").GetComponent<PressButton>();
                    fire.fireButtonDrag = false;
                    


                }
                else
                {
                    enemyMove = false;
                    playerMove = true;
                    timeMove = setTime;


                }
            }
        }
        else
        {
            timeMove = 0f;
            gameOver = true;
        }
    }
    void ActiveButton()
    {
        if (enemyMove)
        {
            PressButton fire = GameObject.FindGameObjectWithTag("FireButton").GetComponent<PressButton>();
            fire.dragBazooka = false;
        }

    }
   
}
