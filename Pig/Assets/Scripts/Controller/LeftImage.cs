﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftImage : MonoBehaviour
{
    [SerializeField] Sprite _Left_Image;
    public void MoveLeft()
    {
        this.GetComponent<SpriteRenderer>().sprite = _Left_Image;
    }
}
