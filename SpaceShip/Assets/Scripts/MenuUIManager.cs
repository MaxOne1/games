﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuUIManager : MonoBehaviour
{
    [SerializeField] private Texture2D _Cursor;
    [SerializeField] private TextMeshProUGUI _Score_Text;
    private int _Score;

    private void Start()
    {
        _Score = PlayerPrefs.GetInt("Score");
        DataText();
        Cursor.SetCursor(_Cursor, Vector2.zero, CursorMode.ForceSoftware);

    }
    public void StartGame()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }
    public void DataText()
    {
        _Score_Text.text = "" + _Score;
    }
   
    public void Quit()
    {
        Application.Quit();
    }
}
