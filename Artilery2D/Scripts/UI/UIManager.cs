﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.GlobalIllumination;

public class UIManager : MonoBehaviour
{
    public Transform weaponMenu;

    private bool weaponMenuActive;

    public float duration;

    public bool riffle;
    public bool shotGun;
    public bool sniperRifle;
    public bool bazooka;

    public GameObject menuPanel;
    public bool menuActive;
    public GameObject levelPanel;
    
    public SpriteRenderer player;

    void Start()
    {
        weaponMenuActive = false;
        if(levelPanel != null)
        levelPanel.SetActive(false);

        menuPanel.SetActive(false);
        player = GetComponent<SpriteRenderer>();
        bazooka = true;
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void WeaponMenuActive()
    {
        if (!weaponMenuActive)
        {
            weaponMenuActive = true;
            weaponMenu.transform.DOMoveY(124, duration);
        }
        else
            WeaponMenuNull();
             
    }
    public void WeaponMenuNull()
    {
        weaponMenuActive = false;
        weaponMenu.transform.DOMoveY(121, duration);
    }

    public void Rifle()
    {
        riffle = true;
        sniperRifle = false;
        shotGun = false;
        bazooka = false;
        WeaponMenuNull();
       
    }
    public void ShotGun()
    {
        riffle = false;
        sniperRifle = false;
        shotGun = true;
        bazooka = false;
        WeaponMenuNull();
        
    }
    public void SniperRifle()
    {
        riffle = false;
        sniperRifle = true;
        shotGun = false;
        bazooka = false;
        WeaponMenuNull();
       
    }
    public void Bazooka()
    {
        riffle = false;
        sniperRifle = false;
        shotGun = false;
        bazooka = true;
        WeaponMenuNull();
        
    }
    public void LevePanel()
    {
        if(levelPanel != null)
        levelPanel.SetActive(true);
    }
    public void LevelOne()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }
    public void LevelTwo()
    {
        SceneManager.LoadScene(2);
        Time.timeScale = 1;
    }
    public void MenuActive()
    {               
            menuPanel.SetActive(true);
            Time.timeScale = 0;
        
    }
    public void MenuNull()
    {        
           menuPanel.SetActive(false);
            Time.timeScale = 1;
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void ChooseColor()
    {
        player.color = new Color(255, 30, 0);

    }
    public void ToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
