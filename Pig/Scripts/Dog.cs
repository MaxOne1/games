﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : UI
{
    [SerializeField] private Transform[] targetPos;
    [SerializeField] private int index;
    [SerializeField] private float speed;
    [SerializeField] Sprite up;
    [SerializeField] Sprite down;
    [SerializeField] Sprite right;
    [SerializeField] Sprite left;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {      
       Move();
    }

    private void Move()
    {
        if (!gameOver)
        {
            transform.position = Vector2.MoveTowards(transform.position, targetPos[index].position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, targetPos[index].position) < 0.01f)
            {

                index++;
                if (index == 1)
                    this.GetComponent<SpriteRenderer>().sprite = right;

                if (index == 2)
                    this.GetComponent<SpriteRenderer>().sprite = down;

                if (index == 3)
                    this.GetComponent<SpriteRenderer>().sprite = left;

                if (index == targetPos.Length)
                {
                    this.GetComponent<SpriteRenderer>().sprite = up;
                    index = 0;

                }
            }
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            EndPanel();
        }
    }
}
