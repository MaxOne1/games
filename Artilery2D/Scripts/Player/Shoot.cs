﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Shoot : MonoBehaviour
{
    private float nextFire;
    public float fireRate;
    private float force;

    public AudioClip rifle;
    private AudioSource audioController;
    public GameObject bullet;

    public Transform muzzle;
    public Transform shotGunmuzzle1;
    public Transform shotGunmuzzle2;
    public TextMeshProUGUI bulletCountText;

    public int bulletCount;
    private int startBulletCount;

    private UIManager uiManage;

    private Controller contrScript;

    private PressButton buttonPress;

    public GameObject line;

    private TimeManage timeScript;
    public Image forceBar;
    public GameObject aim;

    void Start()
    {
        contrScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Controller>();
        audioController = GetComponent<AudioSource>();
        timeScript = GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManage>();
        uiManage = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        buttonPress = GameObject.FindGameObjectWithTag("FireButton").GetComponent<PressButton>();
        startBulletCount = bulletCount;
        force = 0;
        aim.SetActive(false);
    }
    void Update()
    {     if (bulletCount <= 0)
        {
            timeScript.playerMove = true;
            timeScript.timeMove = 0;
        }       
        RifleGun();           
        ShotGun();           
        SniperRifleGun();
        Bazooka();
        BulletCountFull();
        BulletCount();
        ForceBar();
    }
    public void ForceBar()
    {
        if (uiManage.bazooka)
        {
            forceBar.fillAmount = force / 50;
        }
       
        
    }

    
    public void ShotGun()
    {
        if (timeScript.playerMove && !timeScript.gameOver)
        {
            if (contrScript.aimActive)
                aim.SetActive(true);
            else
                aim.SetActive(false);
        }
        if (Time.time > nextFire && bulletCount != 0 && contrScript.isGround && uiManage.shotGun && buttonPress.fireButtonDrag && timeScript.playerMove)
        {
            contrScript.speedX = 0;
            nextFire = Time.time + 1 / fireRate;
            audioController.PlayOneShot(rifle);
            Instantiate(bullet, muzzle.position, muzzle.rotation);
            Instantiate(bullet, shotGunmuzzle1.position, shotGunmuzzle1.rotation);
            Instantiate(bullet, shotGunmuzzle2.position, shotGunmuzzle2.rotation);
            bulletCount--;
        }       
    }
    
    public void RifleGun()
    {
        if (timeScript.playerMove && !timeScript.gameOver)
        {
            if (contrScript.aimActive)
                aim.SetActive(true);
            else
                aim.SetActive(false);
        }
        if (Time.time > nextFire && bulletCount != 0 && contrScript.isGround&& uiManage.riffle && buttonPress.fireButtonDrag && timeScript.playerMove)
        {
            contrScript.speedX = 0;
            nextFire = Time.time + 1 / fireRate;
            audioController.PlayOneShot(rifle);                                      
            Instantiate(bullet, muzzle.position, muzzle.rotation);
            bulletCount--;
        }     
    }
    public void SniperRifleGun()
    {
        aim.SetActive(false);
        if (Time.time > nextFire && bulletCount != 0 && contrScript.isGround&& uiManage.sniperRifle && buttonPress.fireButtonDrag && timeScript.playerMove)
        {

            contrScript.speedX = 0;
            nextFire = Time.time + 1 / fireRate;
            audioController.PlayOneShot(rifle);                          
            Instantiate(bullet, muzzle.position, muzzle.rotation);
            bulletCount--;
        }    
    }
    public void Bazooka()
    { if(timeScript.playerMove && !timeScript.gameOver)
        {
            if (contrScript.aimActive)
                aim.SetActive(true);
            else
                aim.SetActive(false);
        }
       
        if (buttonPress.fireButtonDrag && timeScript.playerMove)
        {
            force += 0.5f;
            if (force >= 50)
            {
                contrScript.speedX = 0;
                nextFire = Time.time + 1 / fireRate;
                audioController.PlayOneShot(rifle);
                Rigidbody2D bulletCreate = Instantiate(bullet, muzzle.position, muzzle.rotation).GetComponent<Rigidbody2D>();
                bulletCreate.AddForce(transform.right * 50, ForceMode2D.Impulse);
                bulletCount--;
                buttonPress.dragBazooka = false;
                force = 0;
            }
        }
        if (timeScript.enemyMove)
            force = 0;
        


        if (Time.time > nextFire &&bulletCount != 0 && contrScript.isGround && uiManage.bazooka && buttonPress.dragBazooka && timeScript.playerMove)
        {
            
            contrScript.speedX = 0;
            nextFire = Time.time + 1 / fireRate;
            audioController.PlayOneShot(rifle);
            Rigidbody2D bulletCreate = Instantiate(bullet, muzzle.position, muzzle.rotation).GetComponent<Rigidbody2D>();
            bulletCreate.AddForce(transform.right * force, ForceMode2D.Impulse);
            buttonPress.dragBazooka = false;
            bulletCount--;           
            force = 0;
        }
        
    }
    void BulletCountFull()
    {
        if (timeScript.enemyMove)
            bulletCount = startBulletCount;
    }
    void BulletCount()
    {
        bulletCountText.text = "" + bulletCount;

    }
    
}




