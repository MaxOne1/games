using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Characteristics
{
    [SerializeField] private GameObject _Weapon;
    private Gun _Gun_Script;
    
    private void Start()
    {
        _Gun_Script = _Weapon.GetComponent<Gun>();
    }

    
    public void Detection(GameObject _target)
    {
        transform.LookAt(_target.transform);
        RaycastHit hit;
        if(Physics.Raycast(transform.position,transform.forward,out hit,10))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                Attack(hit.collider.gameObject);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            if (_Health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
    private void Attack(GameObject _target)
    {
        _Gun_Script.Shoot();
    }

}
