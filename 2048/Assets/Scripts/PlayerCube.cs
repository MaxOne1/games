﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class PlayerCube : Cube,IDragHandler,IEndDragHandler
{
    [SerializeField] private float _Speed;
    [SerializeField] private GameObject _Light;
    private Vector3 _Scale = new Vector3(0.65f, 0.65f, 0.65f);
    private TrailRenderer _Trail;
    public bool _Tached = true;

    protected override void Start()
    {
        base.Start();
        _Trail = GetComponent<TrailRenderer>();
        StartScale();
        
    }
    public void OnDrag(PointerEventData eventData)
    {
        MoveSide();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (_Tached)
        {
            Shoot();                     
            _Game_Script.CallCreateFunction();
            Destroy(_Light.gameObject);
            StartCoroutine(CanLoose());
            _Tached = false;
            _Trail.enabled = true;
        }     
    }
    private void Update()
    {
        _Game_Script.Borders(1.2f, this.gameObject);
    }

    private void StartScale()
    {
        transform.DOScale(_Scale, 1f);
        _Trail.enabled = false;
    }
    private void Shoot()
    {
        if (_Tached)
        {
            _Rigidbody.AddForce(Vector3.forward * _Speed, ForceMode.Impulse);           
        }        
    }
    private void MoveSide()
    {
        if (_Tached)
        {          
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;           
            if(Physics.Raycast(ray, out hit))
            {
                transform.position = new Vector3(hit.point.x, transform.position.y, transform.position.z);
            }   
           
        }        
    }
    private IEnumerator CanLoose()
    {
        _Can_Loose = false;
        yield return new WaitForSeconds(0.5f);
        _Can_Loose = true;  
    }

   
}
