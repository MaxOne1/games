﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float _Speed;      
    private AudioSource _Audio_Source;       
    private bool _Move_Up;
    private bool _Move_Down;
    private bool _Move_Right;
    private bool _Move_Left;
     
    private void Start()
    {
        _Audio_Source = GetComponent<AudioSource>();                      
    } 

    private void Update()
    {
        Movement();
    }
    private void Movement()
    {
        if (_Move_Up)
        {
            MoveUp();
        }
        if (_Move_Down)
        {
            MoveDown();
        }
        if (_Move_Left)
        {
            MoveLeft();
        }
        if (_Move_Right)
        {
            MoveRight();
        }
    }

    public void MoveUp()
    {
        _Move_Up = true;      
        transform.Translate(Vector2.up * _Speed * Time.deltaTime);    
        
    }
    public void MoveDown()
    {
        _Move_Down = true;
        transform.Translate(Vector2.down * _Speed * Time.deltaTime);     
    }   
    public void MoveRight()
    {
        _Move_Right = true;
        transform.Translate(Vector2.right * _Speed * Time.deltaTime);      
    }
    public void MoveLeft()
    {
        _Move_Left = true;
        transform.Translate(Vector2.left * _Speed * Time.deltaTime);            
    }
    public void Stop()
    {
        _Audio_Source.Stop();
        _Move_Up = false;
        _Move_Down = false;
        _Move_Right = false;
        _Move_Left = false;
    } 
    public void FeetSound()
    {
        _Audio_Source.Play();
    }
    
}

