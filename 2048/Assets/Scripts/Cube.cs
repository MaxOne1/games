﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Cube : MonoBehaviour
{
    protected Rigidbody _Rigidbody;

    private Vector3 _Direction;
    private Vector3 _Rotation;

    private float _Force = 2.5f;

    [SerializeField] private int _Index;
    [SerializeField] private GameObject _Particle_Object;

    public int _Number;

    protected Game _Game_Script;

    protected bool _Can_Loose = true;
    protected virtual void Start()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        _Game_Script = GameObject.Find("GameManager").GetComponent<Game>();
    }

    private void OnCollisionEnter(Collision collision)
    {
       
        if (collision.gameObject.CompareTag("Cube"))
        {
            Cube _cube_Script = collision.gameObject.GetComponent<Cube>();            
            if(_Number == _cube_Script._Number)
            {
                ChangeNumber(_Game_Script);               
                Impulse();
                CreateExplosion();
                Destroy(collision.gameObject);
                if(_Number == 2048)
                {
                    _Game_Script.Victory();
                }
            }           
        }        
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Border") && _Can_Loose)
        {
            _Game_Script.Defeat();
        }
    }

    private void RandomDirection()
    {
        float _x_direction = Random.Range(-1, 1);      
        float _z_direction = Random.Range(0, 1);      
        float _x_rotation = Random.Range(-1, 1);      
        float _y_rotation = Random.Range(-1, 1);      
        float _z_rotation = Random.Range(-1, 1);      
        _Direction = new Vector3(_x_direction, _Force, _z_direction);
        _Rotation = new Vector3(_x_rotation, _y_rotation, _z_rotation);
    }
    private void Impulse()
    {
        RandomDirection();
        float _duration = 1f;
        float _strenth = 0.25f;
        int _vibrato = 10;
        float _randomness = 90f;           
        transform.DOShakeScale(_duration, _strenth, _vibrato, _randomness);
        _Rigidbody.AddForce(_Direction, ForceMode.Impulse);
        _Rigidbody.AddTorque(_Rotation, ForceMode.Impulse);     
    }
    private void CreateExplosion()
    {
        Instantiate(_Particle_Object, transform.position, Quaternion.identity);
    }
    private void ChangeNumber(Game _game_Script)
    {
        _Index++;
        _Number *= 2;
        _game_Script.ChangeTexture(this.gameObject, _Index);       
    }
}
