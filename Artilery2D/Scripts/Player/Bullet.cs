﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rb;
    public int damage;
    public GameObject effectObject;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }  

    // Update is called once per frame
    void Update()
    {
        rb.velocity = transform.right * speed;
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("Ground"))
        {
            Instantiate(effectObject, transform.position, Quaternion.identity);
            Debug.Log("Ground");
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
        if (collision.CompareTag("Enemy"))
        {
            Instantiate(effectObject, transform.position, Quaternion.identity);
            Debug.Log("Ground");
            Destroy(gameObject);

        }
        if (collision.CompareTag("Static"))
        {
            Instantiate(effectObject, transform.position, Quaternion.identity);
            Debug.Log("Ground");
            Destroy(gameObject);
        }
    }
}

