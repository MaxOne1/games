﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private Animator _Animator;
    public bool Open;
    void Start()
    {
        _Animator = GetComponent<Animator>();
        Open = false;
    }
   
    public void DoorIsOpen()
    {
        if (Open)
        {
            Open = false;
            _Animator.SetBool("isOpen", false);
        }
        else
        {
            Open = true;
            _Animator.SetBool("isOpen", true);
        }
    }
}
