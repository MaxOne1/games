using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    [SerializeField] private List<GameObject> _Weapon;
    public GameObject _Current_Weapon;
    public Gun _Gun_Script;
    private UI _UI_Script;

    private void Start()
    {
        WeaponChek();
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
    }
    void Update()
    {
        ChangeWeapon();
    }
    private void ChangeWeapon()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && _Weapon[0] != null)
        {
            _Current_Weapon = _Weapon[0];
            _Gun_Script = _Current_Weapon.GetComponent<Gun>();
            _UI_Script.AmmunitionCount(_Current_Weapon.GetComponent<Gun>()._Ammunition);
            for(int i = 0; i < _Weapon.Count; i++)
            {
                _Weapon[i].SetActive(false);
            }
            _Current_Weapon.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) && _Weapon[1] != null)
        {
            _Current_Weapon = _Weapon[1];
            _Gun_Script = _Current_Weapon.GetComponent<Gun>();
            _UI_Script.AmmunitionCount(_Current_Weapon.GetComponent<Gun>()._Ammunition);
            for (int i = 0; i < _Weapon.Count; i++)
            {
                _Weapon[i].SetActive(false);
            }
            _Current_Weapon.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) && _Weapon[2] != null)
        {
            _Current_Weapon = _Weapon[2];
            _Gun_Script = _Current_Weapon.GetComponent<Gun>();
            _UI_Script.AmmunitionCount(_Current_Weapon.GetComponent<Gun>()._Ammunition);
            for (int i = 0; i < _Weapon.Count; i++)
            {
                _Weapon[i].SetActive(false);
            }
            _Current_Weapon.SetActive(true);
        }
    }
    private void WeaponChek()
    {
        for(int i = 0; i < _Weapon.Count; i++)
        {
            if(_Weapon[i] != null)
            {
                _Current_Weapon = _Weapon[i];
                _Gun_Script = _Current_Weapon.GetComponent<Gun>();
                _Current_Weapon.SetActive(true);
                break;
            }
        }
    }  
}
