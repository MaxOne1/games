﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shakeNumb = 0.2f;
    public float shakeDown = 1;
    public Transform camTransform;
    public float shakeDuration = 0;
    Vector3 originPos;
    void Awake()
    {
      if(camTransform == null)
        {
            camTransform = GetComponent(typeof(Transform)) as Transform;
        }  
    }
    void OnEnable()
    {
        originPos = camTransform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if(shakeDuration > 0)
        {
            camTransform.localPosition = originPos + Random.insideUnitSphere * shakeDuration;
            shakeDuration -= Time.deltaTime * shakeDown;
        }
        else
        {
            shakeDuration = 0f;
            camTransform.localPosition = originPos;
        }
        
    }
}
