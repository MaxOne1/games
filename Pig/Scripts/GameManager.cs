﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private float bombTime;
    [SerializeField] private GameObject bombPrefab;
    [SerializeField] private Transform[] bombPlace;
    private Coroutine play;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (play == null)
            play = StartCoroutine(BombCreate());
        
    }
    IEnumerator BombCreate()
    {
        int index = Random.Range(0, bombPlace.Length);
        yield return new WaitForSeconds(bombTime);
        Instantiate(bombPrefab, bombPlace[index].position, Quaternion.identity);
        play = null;
    }
}
