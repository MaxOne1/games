using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionZone : MonoBehaviour
{
    private Enemy _Enemy_Script;

    private void Start()
    {
        _Enemy_Script = gameObject.GetComponentInParent(typeof(Enemy)) as Enemy;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _Enemy_Script.Detection(other.gameObject);
        }
    }
}
