﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ActiveButton : MonoBehaviour
{
    public TimeManage timeManageScript;
    public Button fire;
    public Button weapon;
   

    void Start()
    {    
    }

    // Update is called once per frame
    void Update()
    {
        ActiveButtons();
    }
    public void ActiveButtons()
    {
        if (timeManageScript.playerMove)
        {
            fire.transform.DOMoveX(16.5f, 1);
            weapon.interactable = true;
        }

        if(timeManageScript.enemyMove)
        {
            fire.transform.DOMoveX(25, 1);
            weapon.interactable = false;
        }
            
    }
}

