﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    
    [SerializeField] private float _Offset;
    [SerializeField] private float _Speed;
    [SerializeField] private int _Health = 3;
    [SerializeField] private GameObject _Bullet;
    [SerializeField] private Transform _Gun;
    [SerializeField] private GameObject[] _Trails;
    [SerializeField] private GameObject[] _Bonus_Guns;
    [SerializeField] private GameObject _Explosion;
    private GameUIManager _UI_Script;
    private GameManager _Manager_Script;
    private Rigidbody2D _Rigidbody;
    private SpriteRenderer _Player;
    private Vector3 _Mouse;
    private bool _Double_Fire_Bonus = false;

    private void Start()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Player = GetComponent<SpriteRenderer>();
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<GameUIManager>();
        _Manager_Script = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    private void Update()
    {
        if(Time.timeScale != 0)
        {
            Rotation();
            Movement();
            Shoot();
            _Manager_Script.Border("Player", this.gameObject);
        }      
    }
    private void Rotation()
    {
        _Mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float _rotateZ = Mathf.Atan2(_Mouse.y, _Mouse.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, _rotateZ + _Offset);
    }
    private void Movement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            _Rigidbody.AddForce(Vector3.up * _Speed * Time.deltaTime, ForceMode2D.Impulse);
        }
        if (Input.GetKey(KeyCode.A))
        {
            _Rigidbody.AddForce(Vector3.left * _Speed * Time.deltaTime, ForceMode2D.Impulse);
        }
        if (Input.GetKey(KeyCode.D))
        {
            _Rigidbody.AddForce(Vector3.right * _Speed * Time.deltaTime, ForceMode2D.Impulse);
        }
    }
    private void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(_Bullet, _Gun.position, _Gun.rotation);
            if (_Double_Fire_Bonus)
            {
                Instantiate(_Bullet, _Bonus_Guns[0].transform.position, _Bonus_Guns[0].transform.rotation);
                Instantiate(_Bullet, _Bonus_Guns[1].transform.position, _Bonus_Guns[1].transform.rotation);
            }           
        }
    }   
    private void OnTriggerEnter2D(Collider2D collision)
    {       
        if (collision.CompareTag("Bonus"))
        {
            Destroy(collision.gameObject);
            if (collision.name == "DoubleFire(Clone)")
            {
                StartCoroutine(DoubleFire());               
            } 
            if(collision.name == "Health(Clone)")
            {
                AddHealth();
            }
        }
    }
    private void Death()
    {
        _Health--;
        Instantiate(_Explosion, transform.position, Quaternion.identity);
        _UI_Script.Defeat();
        Destroy(gameObject);
    }
    private IEnumerator Blink()
    {
        float _blinkTime = 0.25f;
        yield return new WaitForSeconds(_blinkTime);
        _Player.color = new Color(_Player.color.r, _Player.color.g, _Player.color.b, 0);
        _Trails[0].SetActive(false);
        _Trails[1].SetActive(false);
        yield return new WaitForSeconds(_blinkTime);
        _Player.color = new Color(_Player.color.r, _Player.color.g, _Player.color.b, 255);
        _Trails[0].SetActive(true);
        _Trails[1].SetActive(true);
        yield return new WaitForSeconds(_blinkTime);
        _Player.color = new Color(_Player.color.r, _Player.color.g, _Player.color.b, 0);
        _Trails[0].SetActive(false);
        _Trails[1].SetActive(false);
        yield return new WaitForSeconds(_blinkTime);
        _Player.color = new Color(_Player.color.r, _Player.color.g, _Player.color.b, 255);
        _Trails[0].SetActive(true);
        _Trails[1].SetActive(true);
        yield return new WaitForSeconds(_blinkTime);
        _Player.color = new Color(_Player.color.r, _Player.color.g, _Player.color.b, 0);
        _Trails[0].SetActive(false);
        _Trails[1].SetActive(false);
        yield return new WaitForSeconds(_blinkTime);
        _Player.color = new Color(_Player.color.r, _Player.color.g, _Player.color.b, 255);
        _Trails[0].SetActive(true);
        _Trails[1].SetActive(true);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Asteroid"))
        {
            StartCoroutine(Blink());
            _UI_Script.HealthPoints();

            if (_Health > 1)
            {
                _Health--;
            }
            else
            {
                Death();
            }
        }
    }
    private IEnumerator DoubleFire()
    {
        float _bonusTime = 10;
        _Double_Fire_Bonus = true;
        yield return new WaitForSeconds(_bonusTime);
        _Double_Fire_Bonus = false;       
    }
    private void AddHealth()
    {
        if (_Health < _UI_Script._Health_Points.Count)
        {
            _Health++;
            _UI_Script._Health_Index--;
            _UI_Script._Health_Points[_UI_Script._Health_Index].enabled = true;           
        }
    }
   
}
