﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float speed;
    public float distanceShoot;
    private float nextFire;
    public float fireRate;
    public float health;
    public int bulletCount;
    private int startBulletCount;
    public Image HPBar;
    public bool isGround;

    public GameObject bullet;

    public Transform player;
    public Transform muzzle;

    private AudioSource aSource;
    public AudioClip shootSound;

    private Rigidbody2D rb;

    private TimeManage timeScript;
    public GameObject victoryObject;

    void Start()
    {
        isGround = true;
        rb = GetComponent<Rigidbody2D>();
        aSource = GetComponent<AudioSource>();
        timeScript = GameObject.Find("TimeManager").GetComponent<TimeManage>();
        startBulletCount = bulletCount;
    }

    // Update is called once per frame
    void Update()
    {

        FullBullets();
        AIMove();
        HpBar();
        Borders();

    }

    void Shoot()
    {
        if (Time.time > nextFire && bulletCount != 0)
        {
            nextFire = Time.time + 1 / fireRate;
            aSource.PlayOneShot(shootSound);
            bulletCount--;
            Instantiate(bullet, muzzle.position, muzzle.rotation);
        }
        if (bulletCount <= 0)
        {
            timeScript.timeMove = 0;
            timeScript.enemyMove = true;
        }


    }
    void AIMove()
    {     
        if (player != null)
        {
            RaycastHit2D hit = Physics2D.Raycast(muzzle.position, transform.right, 50);
            float distanceToPlayer = Vector2.Distance(transform.position, player.position);
            float distanceEnemy = 2.1f;
            if (timeScript.enemyMove)
            {
                if (distanceToPlayer > distanceShoot)
                {
                    MoveToPlayer();
                    if (hit.collider != null)
                    {
                        float distanceToObsstacle = Vector2.Distance(transform.position, hit.transform.position);
                        Debug.Log(hit.collider.name);
                        if (distanceToObsstacle <= distanceEnemy && isGround == true)
                        {
                            rb.AddForce(Vector2.up * 8f, ForceMode2D.Impulse);
                        }
                        if (distanceToObsstacle > distanceEnemy)
                        {
                            MoveToPlayer();
                        }
                    }
                    else
                    {
                        MoveToPlayer();
                    }

                }
                else if (distanceToPlayer < distanceShoot)
                {

                    Shoot();
                }
            }
        }
    }

    void MoveToPlayer()
    {
        if (player.position.x > transform.position.x)
        {
            transform.Translate(Vector2.left * -speed * Time.deltaTime);
            transform.rotation = new Quaternion(0, 0, 0, 0);

        }

        else
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            transform.rotation = new Quaternion(0, 180, 0, 0);

        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            health -= collision.GetComponent<Bullet>().damage;
        }
        if (collision.CompareTag("Bomb"))
        {
            health -= collision.GetComponent<BazookaBullet>().damage;
        }
        if (collision.CompareTag("Ground"))
            isGround = true;
    }
    void Die()
    {
        if (health <= 0)
        {
            Instantiate(victoryObject, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

    }
    void FullBullets()
    {
        if (timeScript.playerMove)
            bulletCount = startBulletCount;

    }
    void HpBar()
    {
        HPBar.fillAmount = health / 100;
        if (HPBar.fillAmount <= 100)
            Die();
        HPBar.transform.position = this.gameObject.transform.position + new Vector3(0, 3.5f, 0);
    }
    void Borders()
    {
        if (transform.position.y < 115 || transform.position.y > 150 || transform.position.x > 25 || transform.position.x < -25)
        {
            health = 0;
        }


    }
}
