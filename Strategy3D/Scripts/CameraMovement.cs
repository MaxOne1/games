﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Camera _Camera;
    [SerializeField] private float _Speed;
    [SerializeField] private VariableJoystick _Joystick;
    private float _Move_H;
    private float _Move_V;
   
    private void Update()
    {
        Movement();
    }
    private void Movement()
    {
            _Move_H = _Joystick.Horizontal;
            _Move_V = _Joystick.Vertical;
            Vector3 _movement = new Vector3(_Move_H, 0, _Move_V);
            _Camera.transform.Translate(_movement * _Speed * Time.deltaTime,Space.World);       
    }
}
