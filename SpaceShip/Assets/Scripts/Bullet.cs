﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _Speed;
    private GameManager _Manager_Script;
    private void Start()
    {
        _Manager_Script = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    private void Update()
    {
        transform.Translate(Vector2.up * _Speed * Time.deltaTime);
        _Manager_Script.Border("Bullet", this.gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Asteroid"))
        {
            _Manager_Script._Asteroids_On_Scene.Remove(collision.gameObject);                      
            collision.GetComponent<Asteroid>().Death();
            _Manager_Script.CreateAsteroids();
            Destroy(gameObject);            
        }
    }
    
}
