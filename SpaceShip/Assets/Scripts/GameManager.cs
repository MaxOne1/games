﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float _Asteroids_Speed;
    public int _Score = 0;
    [SerializeField] private GameObject _Default_Player_Ship;
    [SerializeField] private List<Vector2> _Instantiate_Pos;
    [SerializeField] private List<GameObject> _Asteroids;
    [SerializeField] private List<GameObject> _Bonuses;
    public List<GameObject> _Asteroids_On_Scene;
    private GameObject _Player_Ship; 

    private void Start()
    {
        InvokeRepeating("CreateBonus", 10, 90);
        if(ShipSelection._Player_Ship != null)
        {
            _Player_Ship = ShipSelection._Player_Ship;
            Instantiate(_Player_Ship, Vector2.zero, _Player_Ship.transform.rotation);
        }
        else
        {
            _Player_Ship = _Default_Player_Ship;
            Instantiate(_Player_Ship, Vector2.zero, _Player_Ship.transform.rotation);
        }
        
    }

    public void Border(string _version,GameObject _object )
    {
        Vector3 _camX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 _camY = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        float _leftBorder = _camX.x;
        float _rightBorder = _camY.x;
        float _downBorder = _camX.y;
        float _upBorder = _camY.y;
        if (_version == "Player")
        {
            if (_object.transform.position.x < _leftBorder)
            {
                _object.transform.position = new Vector2(_rightBorder, _object.transform.position.y);
            }
            if (_object.transform.position.x > _rightBorder)
            {
                _object.transform.position = new Vector2(_leftBorder, _object.transform.position.y);
            }
            if (_object.transform.position.y > _upBorder)
            {
                _object.transform.position = new Vector2(_object.transform.position.x, _downBorder);
            }
            if (_object.transform.position.y < _downBorder)
            {
                _object.transform.position = new Vector2(_object.transform.position.x, _upBorder);
            }
        }
        else if(_version == "Asteroid")
        {
            float _addBorder = 0.5f;
            if (_object.transform.position.x < _leftBorder - _addBorder)
            {
                _object.transform.position = new Vector2(_rightBorder + _addBorder, _object.transform.position.y);
            }
            if (_object.transform.position.x > _rightBorder + _addBorder)
            {
                _object.transform.position = new Vector2(_leftBorder - _addBorder, _object.transform.position.y);
            }
            if (_object.transform.position.y > _upBorder + _addBorder)
            {
                _object.transform.position = new Vector2(_object.transform.position.x, _downBorder - _addBorder);
            }
            if (_object.transform.position.y < _downBorder - _addBorder)
            {
                _object.transform.position = new Vector2(_object.transform.position.x, _upBorder + _addBorder);
            }
        }
        else if (_version == "Bullet")
        {
            if (_object.transform.position.x < _leftBorder)
            {
                Destroy(_object.gameObject);
            }
            if (_object.transform.position.x > _rightBorder)
            {
                Destroy(_object.gameObject);
            }
            if (_object.transform.position.y > _upBorder)
            {
                Destroy(_object.gameObject);
            }
            if (_object.transform.position.y < _downBorder)
            {
                Destroy(_object.gameObject);
            }
        }               
    }
    public void CreateAsteroids()
    {
        Vector3 _camX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 _camY = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        float _leftBorder = _camX.x;
        float _rightBorder = _camY.x;
        float _downBorder = _camX.y;
        float _upBorder = _camY.y;
        float _distanceToPlayer = 2f;

        if(_Player_Ship != null)
        {
            Vector2 _positionOne = new Vector2(Random.Range(_leftBorder, _Player_Ship.transform.position.x - _distanceToPlayer), Random.Range(_Player_Ship.transform.position.y - _distanceToPlayer, _downBorder));
            Vector2 _positionTwo = new Vector2(Random.Range(_Player_Ship.transform.position.x + _distanceToPlayer, _rightBorder), Random.Range(_upBorder, _Player_Ship.transform.position.y + _distanceToPlayer));
            _Instantiate_Pos[0] = _positionOne;
            _Instantiate_Pos[1] = _positionTwo;
            int _asteroidsIndex = Random.Range(0, _Asteroids.Count);
            int _positionIndex = Random.Range(0, _Instantiate_Pos.Count);
           GameObject _asteroid = Instantiate(_Asteroids[_asteroidsIndex], _Instantiate_Pos[_positionIndex], _Asteroids[_asteroidsIndex].transform.rotation);
            _Asteroids_On_Scene.Add(_asteroid);
        }     
    }
    public void Score(int _points)
    {
        _Score += _points;
        if (_Score > PlayerPrefs.GetInt("Score"))
        {
            PlayerPrefs.SetInt("Score", _Score);
        }
    }   
    public void CreateBonus()
    {
        Vector3 _camX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 _camY = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        float _leftBorder = _camX.x;
        float _rightBorder = _camY.x;
        float _downBorder = _camX.y;
        float _upBorder = _camY.y;

        Vector2 _position = new Vector2(Random.Range(_leftBorder, _rightBorder), Random.Range(_downBorder, _upBorder));
        int _index = Random.Range(0, _Bonuses.Count);
        Instantiate(_Bonuses[_index], _position, _Bonuses[_index].transform.rotation);
    }
   
}
