﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameObject _Player_Ship;

    public void OnPointerClick(PointerEventData eventData)
    {
        SelectShip();
    }

    public void SelectShip()
    {
        ShipSelection._Player_Ship = _Player_Ship;
    }
}
