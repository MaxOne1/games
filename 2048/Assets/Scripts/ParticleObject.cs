﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleObject : MonoBehaviour
{
    [SerializeField] private ParticleSystem _Explosion;
    private void Start()
    {
        _Explosion.Play();       
    }
    private void Update()
    {
        Destroy(gameObject, _Explosion.duration);
    }


}
