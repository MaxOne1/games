﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Farmer : UI
{
    [SerializeField] protected Transform[] targetPos;
    [SerializeField]private float timeStay;
    [SerializeField]private float setTime;
    [SerializeField]private float speed;
    [SerializeField]protected int index;
    [SerializeField] protected GameObject player;
    [SerializeField] protected GameObject farmer;
    [SerializeField] private Image HPBar;
    [SerializeField] Sprite up;
    [SerializeField] Sprite down;
    [SerializeField] Sprite right;
    [SerializeField] Sprite left;
    [SerializeField] Sprite looseFarmer;
    public float health;
      
    void Start()
    {
        index = 3;
        farmer = this.gameObject;
        setTime = timeStay;
        health = 100;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Die();
    } 
    protected void Move()       
    {
        if (!gameOver)
        {
            transform.position = Vector2.MoveTowards(transform.position, targetPos[index].position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, targetPos[index].position) < 0.01f)
            {
                timeStay -= Time.deltaTime;
                if (timeStay <= 0)
                {
                    index++;
                    timeStay = setTime;
                    if (index == targetPos.Length)
                    {
                        index = 0;
                        this.GetComponent<SpriteRenderer>().sprite = down;
                    }
                        
                }

            }
            if (index == 0||index == 1||index == 5||index == 9)
            {
                this.GetComponent<SpriteRenderer>().sprite = down;
            }
            if (index == 2||index == 6||index == 10)
            {
                this.GetComponent<SpriteRenderer>().sprite = right;
            }
            if (index == 4||index == 8||index == 12)
            {
                this.GetComponent<SpriteRenderer>().sprite = left;
            }
            if (index == 3||index == 7||index == 11)
            {
                this.GetComponent<SpriteRenderer>().sprite = up;
            }          

        }
           
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            EndPanel();  
        }
    }
    void Die()
    {
        HPBar.fillAmount = health / 100;
        if (health <= 0)
        {
            this.GetComponent<SpriteRenderer>().sprite = looseFarmer;
            WinPanel();
        }
            
    }
}
