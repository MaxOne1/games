﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightImage : MonoBehaviour
{
    [SerializeField] Sprite _Right_Image;
    public void MoveRight()
    {
        this.GetComponent<SpriteRenderer>().sprite = _Right_Image;
    }
}
