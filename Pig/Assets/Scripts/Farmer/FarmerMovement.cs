﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FarmerMovement : MonoBehaviour
{
    [SerializeField] private Transform[] _Target_Position;
    [SerializeField] private float _Time_Stay;    
    [SerializeField] private float _Speed;
    [SerializeField] private int _Index;
    [SerializeField] private Sprite _Defeat_Farmer;
    [SerializeField] private float _Health;
    private FarmerImage _Farmer_Image_Script;
    private UI _UI_Script;
    private GameManager _Manager_Script;
    private float _Set_Time;
    
      
    void Start()
    {
        _Manager_Script = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
        _Farmer_Image_Script = GetComponent<FarmerImage>();
        _Index = 3;
        _Set_Time = _Time_Stay;
        _Health = 100;
    }

    void Update()
    {
        Move();       
    } 
    protected void Move()       
    {           
        transform.position = Vector2.MoveTowards(transform.position, _Target_Position[_Index].position, _Speed * Time.deltaTime);
        _Farmer_Image_Script.ChangeDiretion(_Target_Position, _Index);
        if (Vector2.Distance(transform.position, _Target_Position[_Index].position) <= 0f)
        {
           _Time_Stay -= Time.deltaTime;
           if (_Time_Stay <= 0)
           {
              _Index++;
                _Time_Stay = _Set_Time;
              if (_Index == _Target_Position.Length)
              {
                  _Index = 0;
              }                                     
           }          
        }                 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _UI_Script.PlayerDefeat();
            _Manager_Script.StopGame();
        }
    }
    private void Defeat()
    {
        GetComponent<SpriteRenderer>().sprite = _Defeat_Farmer;
        _UI_Script.PlayerVictory();
    }
    public void TakeDamage(float _damage)
    {       
        _Health -= _damage;
        _UI_Script._Farmer_Health(_Health);
        if (_Health <= 0)
        {
            Defeat();
        }
    }    
}
