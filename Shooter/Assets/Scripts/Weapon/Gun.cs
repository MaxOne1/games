using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] protected float _Fire_Rate;
    [SerializeField] protected GameObject _Bullet;
    [SerializeField] protected Transform _Muzzle;
    [SerializeField] private AudioClip _Fire_Sound;   
    [SerializeField] private float _RechargeTime;
    public int _Ammunition;
    private int _Start_Ammunition;
    private float _Default_Bullet_Spread = 0;    
    private AudioSource _Audio_Source;
    protected float _Next_Fire = 0;
    protected UI _UI_Script;

    private void Start()
    {
        _Audio_Source = GetComponent<AudioSource>();
        _UI_Script = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
        _Start_Ammunition = _Ammunition;
    }
   
    protected void FireSound()
    {
        _Audio_Source.PlayOneShot(_Fire_Sound);
    }
    protected virtual void BulletSpread(float _bullet_spread)
    {
        float _spread = Random.Range(-_bullet_spread, _bullet_spread);
        Vector3 _random = new Vector3(_spread, _spread, 0);
        _Muzzle.localEulerAngles = _random;
        
    }

    public virtual void Shoot()
    {
        if (Time.time > _Next_Fire && _Ammunition > 0)
        {
            _Next_Fire = Time.time + 1f / _Fire_Rate;
            BulletSpread(_Default_Bullet_Spread);
            Instantiate(_Bullet, _Muzzle.position, _Muzzle.rotation);
            _Ammunition--;
            FireSound();           
        }  
        if(_Ammunition <= 0)
        {
            StartCoroutine(Recharge());
        }
    }  
    protected IEnumerator Recharge()
    {
        yield return new WaitForSeconds(_RechargeTime);
        _Ammunition = _Start_Ammunition;
    }
   
}
