﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour, IDragHandler
{  
    public GameObject _All_Cubes;
    public GameObject _Player;
    public Transform _Spawn_Pos;
    [SerializeField] private float _Speed;
    [SerializeField] private float _Borders_Plus; 
    [SerializeField] private float _Borders_Minus; 
    private float _Mouse_Pos;
   
    public void OnDrag(PointerEventData eventData)
    {
        _Mouse_Pos += Input.GetTouch(0).deltaPosition.x * _Speed * Time.deltaTime;
        _All_Cubes.transform.localPosition = new Vector3(_Mouse_Pos, _All_Cubes.transform.localPosition.y, _All_Cubes.transform.localPosition.z);      
    }
    private void Update()
    {
        if(_All_Cubes.transform.localPosition.x > _Borders_Plus)
        {
            _All_Cubes.transform.localPosition = new Vector3(_Borders_Plus, _All_Cubes.transform.localPosition.y, _All_Cubes.transform.localPosition.z);
        }
        if (_All_Cubes.transform.localPosition.x < _Borders_Minus)
        {
            _All_Cubes.transform.localPosition = new Vector3(_Borders_Minus, _All_Cubes.transform.localPosition.y, _All_Cubes.transform.localPosition.z);
        }
    }

}
