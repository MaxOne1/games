﻿using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;
using UnityStandardAssets.Characters.FirstPerson;

public class Enemy : MonoBehaviour
{
    [SerializeField] private AudioClip[] _Sounds;   
    [SerializeField] private float _Speed;
    [SerializeField] private RangeSensor _Sensor;
    [SerializeField] private List<Transform> _Way_Points;
    [SerializeField] private AudioClip _Step;
    [SerializeField] private float _Step_Time;
    [SerializeField]private AudioSource _AudioSource;
    private float _Die_Distance = 1.5f;
    private GameObject _Detected;
    private Vector3 _Origin_Pos;
    private NavMeshAgent _Agent;
    private Animator _Animator;
    private bool _Stop_Play;
    private bool _Is_Hunting;
    private int _Cur_Point;
    private UIGameManager _UI_Manager;
    private void Start()
    {
        _Agent = GetComponent<NavMeshAgent>();
        _Animator = GetComponent<Animator>();
        _Agent.enabled = true;
        _UI_Manager = GameObject.Find("UIManager").GetComponent<UIGameManager>();
        _Cur_Point = 0;
        _Stop_Play = false;
        _Is_Hunting = false;
    }

    private void FixedUpdate()
    {
        AI();       
    }


    private void Hunting( GameObject _target)
    {       
        _Is_Hunting = true;
        if (_Is_Hunting)
        {
            _Origin_Pos = transform.position;
            _Animator.SetFloat("Speed", _Speed);
            _Agent.speed = _Speed;
            _Speed = 3.5f;
            _Step_Time = 0.39f;
            _Agent.enabled = true;
            _Agent.destination = _target.transform.position;
            float _Distance = Vector3.Distance(transform.position, _target.transform.position);
            _UI_Manager.DangerMusic();
            if (_Distance <= _Die_Distance)
            {
                _UI_Manager.Death();
            }
        }           
    }
    private void Patrolling()
    {
        if (!_Is_Hunting)
        {
            _Agent.SetDestination(_Way_Points[_Cur_Point].position);
            _UI_Manager.SafeMusic();
            _Animator.SetFloat("Speed", _Speed);
            _Agent.speed = _Speed;
            _Speed = 1.3f;
            _Step_Time = 0.965f;
            if (Vector3.Distance(transform.position, _Way_Points[_Cur_Point].position) <= 1f)
            {
                _Cur_Point++;
                if (_Cur_Point == _Way_Points.Count)
                {
                    _Cur_Point = 0;
                }
            }
        }      
    }
   
    public void StartCoroutineHunt()
    {
        StartCoroutine(HuntingIsContinue());
    }
    private void AI()
    {
       _Detected = _Sensor.GetNearest();
        if (_Detected != null)
        {
            Hunting(_Detected);
        }
        else
        {
            Patrolling();
        }      
    }  
    private IEnumerator HuntingIsContinue()
    {
        float _hunting = 7;
        Hunting(_Detected);
        yield return new WaitForSeconds(_hunting);
        if (_Detected == null)
        {
            _Is_Hunting = false;
        }
        else
        {
            StartCoroutine(HuntingIsContinue());
        }
    }
    public void PlaySteps()
    {
        _AudioSource.GetComponent<AudioSource>().clip = _Step;
        _AudioSource.Play();
    }

}
