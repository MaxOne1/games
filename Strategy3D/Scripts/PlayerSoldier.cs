﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class PlayerSoldier : Warriors
{
    public bool _Is_Clicked;
    private RaycastHit hit;

    void Update()
    {
        if(_Health > 0)
        {
            Movement();
        }      
    }
    private void Movement()
    {
        if (Input.GetMouseButtonDown(0) && _Is_Clicked)
        {           
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);        
            if (Physics.Raycast(ray, out hit, 500))
            {              
                    _Agent.SetDestination(hit.point);
                    _Animator.SetBool("Run", true);                         
            }            
        }
        if (Vector3.Distance(transform.position,hit.point)<=_Agent.stoppingDistance)
        {
            _Animator.SetBool("Run", false);
        }
        else
        {
            _Animator.SetBool("Run", true);
        }
    }
    protected new void Damage()
    {
        base.Damage();
    }   
}
