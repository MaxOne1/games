﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpImage : MonoBehaviour
{
    [SerializeField] Sprite _Up_Image;  
    public void MoveUp()
    {
        this.GetComponent<SpriteRenderer>().sprite = _Up_Image;
    }
}
